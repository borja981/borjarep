--Scripts de ejercicios Borja Blanco Rey

--1. Obtener todos los datos de todos los empleados.
select * from emp;

--2. Obtener todos los datos de todos los departamentos.
select * from dept;

--3. Obtener todos los datos de los administrativos (su trabajo es, en inglés, 'CLERK').
select * from emp where job = 'CLERK';

--4. Ídem, pero ordenado por el nombre.
select * from emp where job = 'CLERK' ORDER BY ENAME ASC;

--5. Obtén el número (código), nombre y salario de los empleados.
select empno as codigo,
ename as nombre,
sal as salario
from emp;

--6. Lista los nombres de todos los departamentos.
select dname from dept;

--7. Ídem, pero ordenándolos por nombre.
select dname from dept order by dname asc;

--8. Ídem, pero ordenándolo por la ciudad (no se debe seleccionar la ciudad en el resultado).
select dname from dept order by location desc;

--9. Ídem, pero el resultado debe mostrarse ordenado por la ciudad en orden inverso.
select dname from dept order by location asc;

--10. Obtener el nombre y empleo de todos los empleados, ordenado por salario.
select ename, job, sal from emp order by sal asc;

--11. Obtener el nombre y empleo de todos los empleados, ordenado primero por su trabajo y luego por su salario
select ename, job  from emp order by job asc, sal asc;

--12. Ídem, pero ordenando inversamente por empleo y normalmente por salario.
select ename, job , sal  from emp order by job desc, sal asc;

--13. Obtén los salarios y las comisiones de los empleados del departamento 30.
select sal, comm from emp where deptno = 30;

--14. Ídem, pero ordenado por comisión.
select sal, comm from emp where deptno = 30 order by comm asc;

--15. a) Obtén las comisiones de todos los empleados. 
select comm from emp;
--(b) Obtén las comisiones de los empleados de forma que no se repitan.
select unique comm from emp;

--16. Obtén el nombre de empleado y su comisión SIN FILAS repetidas.
select unique ename, comm from emp;

--17. Obtén los nombres de los empleados y sus salarios, de forma que no se repitan filas.
select unique ename, sal from emp;

--18. Obtén las comisiones de los empleados y sus números de departamento, de forma que no se repitan filas.
select unique comm, deptno from emp;

--19. Obtén los nuevos salarios de los empleados del departamento 30, que resultarán de sumar a su salario una gratificación de 1000 €.
--Muestra también los nombres de los empleados.
select ename, sal + 1000 from emp where deptno = 30;

--20. Lo mismo que la anterior, pero mostrando también su salario original, y haz que la columna que almacena el nuevo salario se 
--denomine NUEVO SALARIO
select ename, sal ,sal + 1000 as nuevo_salario from emp where deptno = 30;

--21. Halla los empleados que tienen una comisión superior a la mitad de su salario
select * from emp where comm>sal/2;

--22. Halla los empleados que no tienen comisión,
--o que la tengan menor o igual que el 25% de su salario
select * from emp where comm is null or comm = 0 and comm<sal/4;

--23. Obtén una lista de nombres de empleados y sus salarios, de forma que en la salida aparezca en todas las filas “Nombre:" y “Salario:" antes del respectivo campo. 
--Hazlo de forma que selecciones exactamente tres expresiones.
select 'Nombre '||ename||'' as nombre, 'Salario '||sal||''as salario from emp;

--24. Hallar el código, salario y comisión de los empleados cuyo código sea mayor que 7500.
select empno, sal, comm from emp where empno>7500;

--25. Obtén todos los datos de los empleados que estén (considerando una ordenación ASCII por nombre)
--a partir de la J, inclusive.
select * from emp where ename like 'j%';-- Pendiente completar

--26. Obtén el salario, comisión y salario total (salario + comisión) de los empleados con comisión, ordenando el 
--resultado por número de empleado.
select ((CASE WHEN comm  IS NULL THEN 0 else comm  end) +sal)as salario_total
,comm,sal, empno from emp order by empno asc;

--27. Lista la misma información, pero para los empleados que no tienen comisión.
select empno,comm,sal from emp where comm is null or comm =0;

--28. Muestra el nombre de los empleados que, teniendo un salario superior a 1000,
--tengan como jefe al empleado cuyo código es 7698.
select ename from emp where sal > 1000 and mgr = 7698;

--29. Halla el conjunto complementario del resultado del ejercicio anterior.
select ename from emp where sal < 1000 and mgr != 7698;

--30. Indica para cada empleado el porcentaje que supone su comisión sobre su salario,
--ordenando el resultado por el nombre del mismo.
select ename,sal + (CASE WHEN comm  IS NULL THEN 0 else comm  end)/100 as comision
from emp;--Falta terminar. Está mal

--31. Hallar los empleados del departamento 10 cuyo nombre no contiene la cadena LA.
select * from emp where deptno = 10 and ename not like '%LA%';

--32. Obtén los empleados que no son supervisados por ningún otro.
select * from emp where mgr is null;

--33Obtén los nombres de los departamentos que no sean Ventas (SALES) ni investigación (RESEARCH).
--Ordena el resultado por la localidad del departamento.
select * from dept where dname not in('Sales','Research');

--34. Deseamos conocer el nombre de los empleados y el código del departamento 
--de los administrativos (CLERK) que no trabajan en el departamento 10, y cuyo 
--salario es superior a 800, ordenado por fecha de contratación.
select ename, deptno from emp where job = 'CLERK' and deptno !=10 
and sal > 800 order by hiredate asc;

--35. Para los empleados que tengan comisión, obtén sus nombres y el cociente entre
--su salario y su comisión (excepto cuando la comisión sea cero), ordenando 
--el resultado por nombre.
select ename, mod(comm, sal) as cociente from emp where comm !=0;--No estoy seguro si mod es la función

--36. Lista toda la información sobre los empleados cuyo nombre completo tenga exactamente 5 caracteres.
select * from emp where ename like '_____';

--37. Lo mismo, pero para los empleados cuyo nombre tenga al menos cinco letras.
select * from emp where ename like  '%____';

--38. Halla los datos de los empleados que, o bien su nombre empieza por A y su salario es superior a 1000, 
--o bien reciben comisión y trabajan en el departamento 30.
select * from emp where (ename like 'A%' and sal > 1000) or (comm > 0 and deptno = 30);

--39. Halla el nombre, el salario y el sueldo total de todos los empleados, 
--ordenando el resultado primero por salario y luego por el sueldo total.
--En el caso de que no tenga comisión, el sueldo total debe reflejar sólo el salario.
select ename,sal, ((CASE WHEN comm  IS NULL THEN 0 else comm  end) +sal)as salario_total
from emp order by sal asc, salario_total asc;

--40. Obtén el nombre, salario y la comisión de los empleados que perciben un salario 
--que está entre la mitad de la comisión y la propia comisión.
select ename, sal, comm from emp where sal between comm and comm/2;

--41. Obtén el complementario del anterior.
select ename, sal,comm from emp where ename not in 
(select ename from emp where sal between comm and comm/2);
--40 y 41 dar alguna vuelta porque creo que estan mal

--42. Lista los nombres y empleos de aquellos empleados cuyo empleo acaba en MAN y cuyo nombre empieza por A.
select ename, job from emp where job like '%MAN' and ename like 'A%';

--43. Intenta resolver la pregunta anterior con un predicado simple, es decir, de forma que en la cláusula 
--WHERE no haya conectores lógicos como AND, OR, etc. Si ayuda a resolver la pregunta, se puede suponer que el nombre del empleado tiene al menos cinco letras.
select ename, job from emp where ename like 'A____';--Revisar