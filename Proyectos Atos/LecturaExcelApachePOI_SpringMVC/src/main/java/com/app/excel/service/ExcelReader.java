package com.app.excel.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import com.app.excel.model.ExcelDataVO;

@Service
public class ExcelReader {

	private ExcelDataVO excelDataVO;
	private int contador;
	private List<ExcelDataVO> lista = new ArrayList();

	public List<ExcelDataVO> excelImport() throws IOException {
		
		String excelFilePath = "c:/test/miexcel.xls";//Ruta del fichero a procesar
		FileInputStream inputStream = new FileInputStream(new File(excelFilePath));//Se transforma a tipo "File"

		Workbook workbook = new XSSFWorkbook(inputStream);//Se le pasa el excel formateado
		Sheet firstSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = firstSheet.iterator();
		Cell cell;
		Row nextRow;

		while (iterator.hasNext()) {
			nextRow  = iterator.next();
			//El primer while recorre las filas, el segundo las celdas (columnas)
			excelDataVO = new ExcelDataVO();
			Iterator<Cell> cellIterator = nextRow.cellIterator();
			if (nextRow.getPhysicalNumberOfCells() == 4) {
				System.out.println(" -- La fila tiene todas las celdas rellenadas -- ");
			}else {
				System.out.println(" // Esta fila tiene una celda vac�a (no numerica) // ");				
			}
			while (cellIterator.hasNext()) {
				cell = cellIterator.next();
				//Se comprueba si la celda es numerica o cadena de caracteres
				if (cell.getCellType()== CellType.STRING) {
					System.out.println("El valor de la celda "+cell.getColumnIndex()+","
							+ " fila "+nextRow.getRowNum()+" es una cadena");
					cellChecker(cell);
					if(cell.getColumnIndex() == 3) {
						System.out.println("######## Salto de fila ########");
						lista.add(contador,excelDataVO);
						contador = contador + 1;
					}		
				} else if (cell.getCellType() == CellType.NUMERIC) {
					System.out.println("El valor de la celda "+cell.getColumnIndex()+","
							+ " fila "+nextRow.getRowNum()+" es un numerica");
					cellChecker(cell);
				}
			}
		}
		workbook.close();
		inputStream.close();
		return lista;
	}	

	private void cellChecker(Cell cell) {
		//En funcion de la posicion de la celda se saca la columna con el dato a comprobar
		switch (cell.getColumnIndex()) {
		case 0: excelDataVO.setNombre(cell.getStringCellValue());		
		break;
		case 1: excelDataVO.setApellido(cell.getStringCellValue());		
		break;
		case 2: int edad = (int) cell.getNumericCellValue();//Los campos que recoge de las celdas siempre son cadenas
		excelDataVO.setEdad(edad);			
		break;
		case 3: excelDataVO.setCiudad(cell.getStringCellValue());	
		break;
		}
	}	
}


