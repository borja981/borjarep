package com.app.excel.controller;


import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.app.excel.model.ExcelDataVO;
import com.app.excel.service.ExcelReader;

@Controller
public class HomeController {
	
	private ExcelReader excelReader = new ExcelReader();

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView home() throws IOException {
		List<ExcelDataVO> lista = excelReader.excelImport();
		return new ModelAndView("home", "lista", lista);
	}
	
}
