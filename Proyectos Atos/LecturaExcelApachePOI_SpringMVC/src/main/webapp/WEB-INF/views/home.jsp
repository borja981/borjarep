<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%-- Libreria core de JSTL --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%-- Libreria form de Spring MVC --%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<title>ListaExcel</title>
<style>
body {
	font-family: monospace;
	background-color: whitesmoke;
}

th {
	width: 15em;
	background-color: grey;
}
.celda {
	text-align: center;
	background-color: lightgrey;
}
td {
	width: 3em;
}
</style>
</head>
<body>
	<h1>Listado de filas Excel</h1>
	<P></P>

	<table>
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Edad</th>
				<th>Ciudad</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${lista}" var="lista">
				<tr class="celda">
					<td><c:out value="${lista.nombre}" /></td>
					<td><c:out value="${lista.apellido}" /></td>
					<td><c:out value="${lista.edad}" /></td>
					<td><c:out value="${lista.ciudad}" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>

