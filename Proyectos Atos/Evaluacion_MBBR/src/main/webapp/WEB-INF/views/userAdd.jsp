<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%-- Libreria core de JSTL --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%-- Libreria form de Spring MVC --%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add new user</title>
<style>
#desc {
	height: 6em;
	width: 16em;
}

body {
	font-family: monospace;
	background-color: whitesmoke
}

.encabezados {
	font-weight: bold;
	width: 10em;
	color: darkblue
}

#title {
	width: 30em;
}

legend {
	font-weight: bold;
	font-size: 15px;
	color: black
}

textarea {
	height: 6em;
	width: 30em;
}
</style>
</head>
<body>
	<h1>Save or Update view</h1>

	</form>
	<form:form modelAttribute="user" action="userAdd" method="POST">
	<fieldset>
	<legend>User fields</legend>
		<table style="width: 350px; height: 125px">
			<tr>
				<td class="encabezados"><form:label path="idUser">D-Id</form:label></td>
				<td><form:input path="idUser" size="30" maxlength="40"
						value="${user.idUser}" disabled="disabled" /></td>
			</tr>
			<tr>
				<td class="encabezados"><form:label path="userName">Name</form:label></td>
				<td><form:input path="userName" size="50" maxlength="80"
						value="${user.userName}" /></td>
			</tr>
			<tr>
				<td class="encabezados"><form:label path="userSname">Surname</form:label></td>
				<td><form:input path="userSname" size="50" maxlength="80"
						value="${user.userSname}" /></td>
			</tr>
			<tr>
				<td class="encabezados"><form:label path="userMail">Mail@</form:label></td>
				<td><form:input path="userMail" size="50" maxlength="80"
						value="${user.userMail}" /></td>
			</tr>
			<tr>
				<td class="encabezados"><form:label path="userPwd">Password</form:label></td>
				<td><form:input path="userPwd" size="50" maxlength="80"
						value="${user.userPwd}" /></td>
			</tr>
			<tr>
				<td class="encabezados"><form:label path="userStatus">Status</form:label></td>
				<td><form:select path="userStatus" value="${user.userStatus}">
						<form:option value="active" label="active" />
						<form:option value="inactive" label="inactive" />
					</form:select></td>
			</tr>
			<tr>
				<td class="encabezados"><form:label path="userCurrentRol">Rol</form:label></td>
				<td><form:select path="userCurrentRol"
						value="${user.userCurrentRol} }">
						<form:option value="admin" label="admin" />
						<form:option value="user" label="user" />
					</form:select></td>
			</tr>
			</fieldset>
			<tr>
				<td colspan="2"><input type="submit"
					value="   Save or Update  " /></td>
			</tr>
		</table>
	</form:form>

</body>
</html>