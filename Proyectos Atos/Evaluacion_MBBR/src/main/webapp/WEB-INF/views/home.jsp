<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<style>
body {
	font-family: monospace;
	background-color: whitesmoke;
}

#encabezados {
	background-color: darkgrey;
	width: 9em
}

#desc {
	background-color: lightgrey;;
}
</style>
<title>Home</title>
</head>
<body>
	<h1>Sign in to access</h1>

	<% if(request.getAttribute("msg") != null) { %>
	<font color="red"> <%= request.getAttribute("msg") %>
	</font>
	<% } %>

	<form id="login" method="post" action="login">
<fieldset>
            <legend>Login</legend>
		<table>
			<tr>
				<td id="encabezados"><label>DasID</label></td>
				<td id="desc"><input type="text" size="20" name="idUser" /></td>
			</tr>
			<tr>
				<td id="encabezados"><label>Password</label></td>
				<td id="desc"><input type="text" size="20" name="userPwd" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="Enter" /></td>
			</tr>
		</table>
</fieldset>
	</form>
	<font color="green">Please, if you install the app for first time you must remember that in src file is database source file.
	Current list view page size 2, you can change this value in implementation methods
</body>
</html>
