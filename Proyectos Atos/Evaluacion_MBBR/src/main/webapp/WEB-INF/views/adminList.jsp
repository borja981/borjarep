<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%-- Libreria core de JSTL --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%-- Libreria form de Spring MVC --%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
body {
	font-family: monospace;
	background-color: whitesmoke;
}

th {
	width: 15em;
	background-color: grey;
}

.celda {
	text-align: center;
	background-color: lightgrey;
}

td {
	width: 3em;
}
</style>
<script>
	function alert() {
		confirm("Delete user?")
	}
</script>
</head>
<body>
	<h1>Admin List view</h1>
	<h3>
		Current user DASID: <font color="green"> <%=request.getSession().getAttribute("idUser")%>
		</font>
	</h3>
	<p>
		<%
			if (request.getAttribute("msg") != null) {
		%>
		<font color="red"> <%=request.getAttribute("msg")%>
		</font>
		<%
			}
		%>
	</p>
	<fieldset>
		<legend>Links</legend>
		<a href="<c:url value='form' />">add user</a> <a
			href="<c:url value='backHome' />">Log-out</a>
	</fieldset>
	</br>
	<fieldset>
		<table>

			<thead>
				<tr>
					<th>D-Id</th>
					<th>Name</th>
					<th>Surname</th>
					<th>mail</th>
					<th>Status</th>
					<th>Rol</th>
					<th>Delete</th>
					<th>Modify</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${adminList}" var="user">
					<tr class="celda">
						<td><c:out value="${user.idUser}" /></td>
						<td><c:out value="${user.userName}" /></td>
						<td><c:out value="${user.userSname}" /></td>
						<td><c:out value="${user.userMail}" /></td>
						<td><c:out value="${user.userStatus}" /></td>
						<td><c:out value="${user.rol.rolName}" /></td>
						<td><c:url value="/deleteUser" var="idUser">
								<c:param name="idUser" value="${user.idUser}" />
							</c:url> <a href='<c:out value="${idUser}"/>' onclick="alert()">Delete
								user</a></td>
						<td><c:url value="/getUser" var="idUser">
								<c:param name="idUser" value="${user.idUser}" />
							</c:url> <a href='<c:out value="${idUser}"/>'>Modify user</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</fieldset>
	<%
		int prev = 0;
	%>
	<%
		int next = 1;
	%>
	<c:url value="/controlPages" var="typeControl">
		<c:param name="typeControl" value="${0}" />
	</c:url>
	<a href='<c:out value="${typeControl}"/>'>Prev Page</a>

	<c:url value="/controlPages" var="typeControl">
		<c:param name="typeControl" value="${1}" />
	</c:url>
	<a href='<c:out value="${typeControl}"/>'>Next Page</a>
</body>
</html>