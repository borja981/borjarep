package org.atos.mbbr.services;

import java.util.List;
import org.atos.mbbr.dao.UserDAO;
import org.atos.mbbr.model.Rol;
import org.atos.mbbr.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDAO dao;
	
	@Transactional(readOnly = true)
	@Override
	public User getById(String idUser) throws Exception {
		return dao.findById(idUser);
	}
	
	@Transactional(readOnly = true)
	@Override
	public List<User> getAll(int controlPage) throws Exception {
		return dao.findAll(controlPage);
	}
	
	@Transactional
	@Override
	public void deleteUser(String idUser) throws Exception {
		dao.deleteUser(idUser);
	}
	
	@Transactional
	@Override
	public void addUser(User user) throws Exception {
		dao.addUser(user);
	}
	
	@Transactional(readOnly = true)
	@Override
	public Rol findRol(String rolName) throws Exception {
		return dao.findRol(rolName);
	}
	
}
