package org.atos.mbbr.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="A_ROL")
public class Rol implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="ID_ROL")
	private int idRol;
	
	@Column(name="ROL_NAME")
	private String rolName;
	
    @OneToMany(mappedBy="rol")
	private List<User> user;
	
    public Rol() {
    	
    }
    
	public Rol(int idRol, String rolName, List<User> user) {
		super();
		this.idRol = idRol;
		this.rolName = rolName;
		this.user = user;
	}

	public int getIdRol() {
		return idRol;
	}

	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}

	public String getRolName() {
		return rolName;
	}

	public void setRolName(String rolName) {
		this.rolName = rolName;
	}

	public List<User> getUser() {
		return user;
	}

	public void setUser(List<User> user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Rol [idRol=" + idRol + ", rolName=" + rolName + ", user=" + user + "]";
	}
    
}
