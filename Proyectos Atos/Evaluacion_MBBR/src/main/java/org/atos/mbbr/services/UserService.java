package org.atos.mbbr.services;

import java.util.List;
import org.atos.mbbr.model.Rol;
import org.atos.mbbr.model.User;

public interface UserService {
	
	User getById(String idUser) throws Exception;
	
	List <User> getAll(int controlPage) throws Exception;
	
	void deleteUser(String idUser) throws Exception;
	
	void addUser(User user) throws Exception;
	
	Rol findRol(String rolName) throws Exception;

}
