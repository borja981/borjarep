package org.atos.mbbr.dao;

import java.util.List;
import javax.persistence.TypedQuery;
import org.atos.mbbr.model.Rol;
import org.atos.mbbr.model.User;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class UserDAOImpl implements UserDAO{

	@Autowired
	//private HttpServletRequest request;
	private SessionFactory sessionFactory;
	private int countPage;

	@Override
	public User findById(String idUser) throws Exception{

		try {
			return sessionFactory.getCurrentSession().find(User.class, idUser);	
		} catch (Exception e) {
			sessionFactory.getCurrentSession().close();
			throw new Exception("Query failed in findById", e);}
	}



	@Override
	public List<User> findAll(int controlPage) throws Exception {
		
		try {
		@SuppressWarnings("unchecked")
		Query<User> query = sessionFactory.getCurrentSession().createQuery("from User");

		int querySize = query.getResultList().size();
		System.out.println("FindAll method -- querySize: "+querySize);
		System.out.println("control page value en findALL method: "+controlPage);

		query.getResultList();
		query.setMaxResults(2);

		if(countPage>=querySize) {
			countPage=0;
		}

		if(countPage<querySize && controlPage==1) {	
			System.out.println("valor control sumar "+countPage);
			query.setFirstResult(countPage);
			countPage = 2+countPage;
			return query.getResultList();
		}

		if(controlPage==0) {
			System.out.println("valor control restar "+countPage);
			countPage--;
			query.setFirstResult(countPage);
			query.getResultList();
			if(countPage<=0) {
				countPage=1;
			}		
		}

		return query.getResultList();
		
		} catch (Exception e) {
			sessionFactory.getCurrentSession().close();
			throw new Exception("Query failed in getAll method", e);}
	
	}

	public void deleteUser(String idUser) throws Exception {
		try {
		User user = sessionFactory.getCurrentSession().find(User.class, idUser);
		sessionFactory.getCurrentSession().delete(user);
		}catch (Exception e) {
			sessionFactory.getCurrentSession().close();
			throw new Exception("Query failed in deleted method", e);}
	}

	public void addUser(User user) {
		sessionFactory.getCurrentSession().saveOrUpdate(user);
	}

	@Override
	public Rol findRol(String rolName) {

		@SuppressWarnings("unchecked")
		TypedQuery<Rol> query = sessionFactory.getCurrentSession().createQuery("from Rol where rolName='"+rolName+"'");
		Rol rol = query.getSingleResult();

		return rol;	
	}

}
