package org.atos.mbbr.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/*
 * Clase de configuracion Spring
 */
@Configuration
/*
 * Habilitar Spring MVC
 */
@EnableWebMvc
/*
 * Habilitar escaneo de componentes Spring para web
 * (@Controller)
 */
@ComponentScan(basePackages={"org.atos.mbbr.web"})

public class WebMvcConfig implements WebMvcConfigurer {

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/static/**").
			addResourceLocations("/resources/");
	}
	
	/*
	 * M�todo para devolver componente (bean) Spring
	 * para ViewResolver
	 */
	@Bean
	public ViewResolver resolver() {
		InternalResourceViewResolver viewResolver = new
				InternalResourceViewResolver();
		
		viewResolver.setSuffix(".jsp");
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setViewClass(JstlView.class);
		
		return viewResolver;
	}
	
	/*
	 * M�todo para devolver componente (bean) Spring
	 * para trabajar con el fichero de propiedades de
	 * nombre messages (fichero messages.properties)
	 * en la aplicacion
	 */
	@Bean
	public MessageSource source() {
		ResourceBundleMessageSource messageSource = new
				ResourceBundleMessageSource();
		
		messageSource.setBasename("messages");
		
		return messageSource;
	}
	
	/*
	 * M�todo para devolver componente (bean) Spring
	 * para validacion mediante Hibernate. Asociamos los posibles
	 * mensajes de error de validacion al fichero de propiedades
	 */
	@Bean
	public Validator getValidator() {
		LocalValidatorFactoryBean validator = new
				LocalValidatorFactoryBean();
		
		validator.setValidationMessageSource(source());
		
		return validator;
	}
}













