package org.atos.mbbr.dao;

import java.util.List;

import org.atos.mbbr.model.Rol;
import org.atos.mbbr.model.User;

public interface UserDAO {
	
	User findById(String idUser) throws Exception;
	
	List<User> findAll(int controlPage) throws Exception;
	
	void deleteUser(String idUser) throws Exception;
	
	void addUser(User user) throws Exception;
	
	Rol findRol(String rolName) throws Exception;

}
