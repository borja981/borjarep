package org.atos.mbbr.config;

import org.atos.mbbr.model.Rol;
import org.atos.mbbr.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;


/*
 * Clase de configuracion Spring
 */
@Configuration
/*
 * Habilitar escaneo de componentes Spring NO web
 * (@Component, @Repository y @Service)
 */
@ComponentScan(basePackages={	"org.atos.mbbr.dao",
								"org.atos.mbbr.services",
								})
/*
 * Habilitar gestion de transacciones de los metodos configurados
 * por anotaciones
 */
@EnableTransactionManagement
public class HibernateConfig {

	/*
	 * Inyectar referencia a la aplicacion de Sprin
	 */
	@Autowired
	private ApplicationContext context;
	
	
	/*
	 * M�todo para devolver componente (bean) Spring
	 * para gestion de sesiones de Hibernate. Enlazamos
	 * para la creaci�n de sesiones el fichero de configuracion
	 * de Hibernate donde se ha configurado SessionFactory
	 * Recuperamos desde la ruta de recursos de la aplicacion
	 */
	@Bean
	public LocalSessionFactoryBean getSessionFactory() {
		LocalSessionFactoryBean factoryBean = new
				LocalSessionFactoryBean();
		
		factoryBean.setConfigLocation(context.getResource(
				"classpath:hibernate.cfg.xml"));
		
		// Especificar clase de entidad a manejar
		factoryBean.setAnnotatedClasses(User.class, Rol.class);
		
		return factoryBean;
	}
	
	/*
	 * M�todo para devolver componente (bean) Spring
	 * para gestion de transacciones de Hibernate. Enlazamos
	 * con el SessionFactory configurado anteriormente
	 */
	@Bean
	public HibernateTransactionManager getTransactionManager() {
		HibernateTransactionManager manager = new
				HibernateTransactionManager();
		
		manager.setSessionFactory(getSessionFactory().getObject());
		
		return manager;
	}
}







