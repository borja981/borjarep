package org.atos.mbbr.web;


import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.atos.mbbr.model.User;
import org.atos.mbbr.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ListController {

	private static final Logger logger = Logger.getLogger(ListController.class);

	@Autowired
	private UserService service;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {

		return "home";
	}

	@RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
	public ModelAndView delete(HttpServletRequest request, Locale locale, Model model, String idUser) throws Exception {
		try {
			logger.info("User delete id: "+idUser );
			logger.info("User logged in current session: "+request.getSession().getAttribute("idUser"));
			if(idUser.equals(request.getSession().getAttribute("idUser"))){
				request.setAttribute("msg","Can not remove current user");
				return new ModelAndView("adminList", "adminList",service.getAll(1));
			}
			service.deleteUser(idUser);
			return new ModelAndView("adminList", "adminList",service.getAll(1));
		}catch (Exception e) {
			
			logger.error(e.getMessage() + "\n");
			e.printStackTrace();
			return new ModelAndView("error");
		}

	}

	@GetMapping (value = "/backHome")
	public String backHome(HttpServletRequest request) {
		String idUser="";
		request.getSession().setAttribute("idUser", idUser);
		return "home";
	}

	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public ModelAndView showForm(HttpServletRequest request) {
		logger.info("show form");
		if(urlAccess(request)) {	
			return new ModelAndView("home");
		}
		return new ModelAndView("userAdd", "user", new User());
	}

	@RequestMapping(value ="/userAdd", method = RequestMethod.POST)
	public ModelAndView addForm(@ModelAttribute("user")HttpServletRequest request, User usu) {
		try {
			if(urlAccess(request)) {	
				return new ModelAndView("home");
			}	
			String rolNameForm = usu.getUserCurrentRol();
			usu.setRol(service.findRol(rolNameForm));
			logger.info("rol format id: "+usu.getRol().getRolName());
			service.addUser(usu);

			return new ModelAndView("adminList", "adminList",service.getAll(1));

		}catch (Exception e) {
			logger.error(e.getMessage() + "\n");
			e.printStackTrace();
			return new ModelAndView("error");
		}
	}


	@RequestMapping(value = "/getUser", method = RequestMethod.GET)
	public ModelAndView getUser(HttpServletRequest request, Locale locale, Model model, String idUser) throws Exception {
		System.out.println("User modify with id: "+idUser );
		try {
			if(urlAccess(request)) {	
				return new ModelAndView("home");
			}
			return new ModelAndView("userAdd", "user",service.getById(idUser));
		}catch (Exception e) {
			logger.error(e.getMessage() + "\n");
			e.printStackTrace();
			return new ModelAndView("error");
		}

	}

	@RequestMapping(value = "/getUser", method = RequestMethod.POST) 
	public ModelAndView modify(HttpServletRequest request, Locale locale, Model model, String idUser) throws Exception {
		System.out.println("User modify with id: "+idUser );
		try {
			if(urlAccess(request)) {	
				return new ModelAndView("home");
			}
		return new ModelAndView("userEdit");
		}catch (Exception e) {
			logger.error(e.getMessage() + "\n");
			e.printStackTrace();
			return new ModelAndView("error");
		}

	}

	@RequestMapping(value = "/controlPages", method = RequestMethod.GET)
	public ModelAndView controlPages(HttpServletRequest request, Locale locale, Model model, int typeControl) throws Exception {
		logger.info("data page control value: "+typeControl);
	
		try {
			if(urlAccess(request)) {	
				return new ModelAndView("home");
			}
		return new ModelAndView("adminList", "adminList",service.getAll(typeControl));
		}catch (Exception e) {
			logger.error(e.getMessage() + "\n");
			e.printStackTrace();
			return new ModelAndView("error");
		}
	}
	/* Method to check the access through url mapping*/
	public boolean urlAccess(HttpServletRequest request) {
		if(request.getSession().getAttribute("idUser").equals("")) {
			logger.info("Not user logged");
			request.setAttribute("msg","Not user logged!!");
			return true;
		}
		return false;
	}

}
