package org.atos.mbbr.web;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.atos.mbbr.model.User;
import org.atos.mbbr.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	private UserService service;


	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {

		return "home";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView start(HttpServletRequest request, @ModelAttribute("userName") String user, Model model) {

		logger.info("NICK {}", user);	
		request.getSession().setAttribute("userName", user);

		return new ModelAndView("userList", "userList",service.getAll());
	}

	@RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
	public ModelAndView delete(Locale locale, Model model, String idUser) {
		System.out.println("User delete id: "+idUser );
		service.deleteUser(idUser);
		return new ModelAndView("userList", "userList",service.getAll());

	}
	@GetMapping (value = "/form")
	public String form() {
		return "userAdd";
	}
	
	@GetMapping (value = "/backHome")
	public String backHome() {
		return "home";
	}
/*
	@RequestMapping(value = "/userAdd", method = RequestMethod.POST)
	public String addUser(@ModelAttribute("newUser") Model model, User user) {
		service.addUser(user);	
		return "redirect:/userList";
	}
 */
@RequestMapping(value = "/userAdd", method = RequestMethod.POST)
public ModelAndView addUser(@RequestParam String idUser) {
	User usu = new User();
	usu.setIdUser(idUser);
	usu.setUserName("Borja");
	usu.setUserSname("Blanco");
	usu.setUserMail("mail@");
	usu.setUserPwd("12234");
	usu.setUserStatus("active");
	usu.setRol(service.findRol("admin"));
	System.out.println("form id: "+idUser);
	System.out.println("rol format id: "+usu.getRol().getRolName());
	service.addUser(usu);

	return new ModelAndView("userList", "userList",service.getAll());

}
}
