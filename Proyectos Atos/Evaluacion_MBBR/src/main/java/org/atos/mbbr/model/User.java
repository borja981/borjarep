package org.atos.mbbr.model;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="A_USER")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_USER")
	private String idUser;

	@Column(name="USER_NAME")
	private String userName;

	@Column(name="USER_SNAME")
	private String userSname;

	@Column(name="USER_MAIL")
	private String userMail;

	@Column(name="USER_PWD")
	private String userPwd;

	@Column(name="USER_STATUS")
	private String userStatus;

	@ManyToOne
	@JoinColumn(name = "id_rolfk")
	private Rol rol;

	/*Variable para coger datos del formulario addUser (String)*/
	@Column(name="USER_CURRENT_ROL")
	private String userCurrentRol;

	public User() {

	}

	public User(String idUser) {
		super();
		this.idUser = idUser;
	}

	public User(String idUser, String userName, String userSname, String userMail, String userPwd, String userStatus,
			Rol rol, String userCurrentRol) {
		super();
		this.idUser = idUser;
		this.userName = userName;
		this.userSname = userSname;
		this.userMail = userMail;
		this.userPwd = userPwd;
		this.userStatus = userStatus;
		this.rol = rol;
		this.userCurrentRol = userCurrentRol;
	}


	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserSname() {
		return userSname;
	}

	public void setUserSname(String userSname) {
		this.userSname = userSname;
	}

	public String getUserMail() {
		return userMail;
	}

	public void setUserMail(String usermail) {
		this.userMail = usermail;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}


	public String getUserStatus() {
		return userStatus;
	}


	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}


	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public String getUserCurrentRol() {
		return userCurrentRol;
	}

	public void setUserCurrentRol(String rolNameFrom) {
		this.userCurrentRol = rolNameFrom;
	}

	@Override
	public String toString() {
		return "User [idUser=" + idUser + ", userName=" + userName + ", userSname=" + userSname + ", userMail="
				+ userMail + ", userPwd=" + userPwd + ", userStatus=" + userStatus + ", rol=" + rol + "]";
	}

}
