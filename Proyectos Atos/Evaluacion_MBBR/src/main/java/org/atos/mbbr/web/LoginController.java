package org.atos.mbbr.web;

import javax.servlet.http.HttpServletRequest;

import org.atos.mbbr.model.User;
import org.atos.mbbr.services.UserService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

	private static final Logger logger = Logger.getLogger(LoginController.class);

	@Autowired
	private UserService service;


	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView start(HttpServletRequest request, @ModelAttribute("idUser") 
	String idUser,@ModelAttribute("userPwd")String pass, Model model) throws Exception {
		
		String passDb;
		String rolNameDb;
		String userStatusDb;
		try {
		logger.info("NICK {} "+idUser +  " PWD {} "+ pass);	
		request.getSession().setAttribute("idUser", idUser);
		
		User user = service.getById(idUser);
		if(user!=null) {
		passDb =  user.getUserPwd();
		rolNameDb = user.getRol().getRolName();
		userStatusDb = user.getUserStatus();
		logger.info("user with pwd: {} "+passDb +"  rol: "+rolNameDb+" Status: "+userStatusDb);
		
			if(passDb.equals(pass) && userStatusDb.equals("active") && rolNameDb.equals("admin")){
				return new ModelAndView("adminList", "adminList",service.getAll(1));
			}
			if(passDb.equals(pass) && userStatusDb.equals("active") && rolNameDb.equals("user")){
				return new ModelAndView("userList", "userList",service.getAll(1));
			}				
		}
		request.setAttribute("msg","User or password incorrect");
		return new ModelAndView("home");
		}catch (Exception e) {
			System.out.println(e.getMessage() + "\n");
			e.printStackTrace();
			return new ModelAndView("error");
		}
	}
}