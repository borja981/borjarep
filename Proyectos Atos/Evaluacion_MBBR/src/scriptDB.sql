--------------------------------------------------------------------------------
--//Script base de datos de evaluacion//--;

create table a_user (
id_user varchar(7) primary key not null,
user_name varchar(100),
user_sname varchar(100),
user_mail varchar(50),
user_pwd varchar(8),
user_status varchar(10),
id_rolfk int REFERENCES a_rol(id_rol));

ALTER TABLE a_user ADD (user_current_rol varchar(10));

create table a_rol (
id_rol int primary key not null,
rol_name varchar (20));

INSERT INTO a_user (id_user,user_name,user_sname,user_mail,user_pwd,user_status,id_rolfk) VALUES('A000001','Borja','Blanco','mail@','abc1234','active',1);
INSERT INTO a_user (id_user,user_name,user_sname,user_mail,user_pwd,user_status,id_rolfk) VALUES('A000002','Ana','Lopez','mail@','abc1234','active',1);
INSERT INTO a_user (id_user,user_name,user_sname,user_mail,user_pwd,user_status,id_rolfk) VALUES('A000003','Pedro','Gil','mail@','abc1234','active',1);
INSERT INTO a_user (id_user,user_name,user_sname,user_mail,user_pwd,user_status,id_rolfk) VALUES('A000004','Juan','Martinez','mail@','abc1234','active',1);
INSERT INTO a_user (id_user,user_name,user_sname,user_mail,user_pwd,user_status,id_rolfk) VALUES('A000005','Manuel','Blanco','mail@','abc1234','active',1);
INSERT INTO a_user (id_user,user_name,user_sname,user_mail,user_pwd,user_status,id_rolfk) VALUES('A000006','Laura','Lopez','mail@','abc1234','active',1);
INSERT INTO a_user (id_user,user_name,user_sname,user_mail,user_pwd,user_status,id_rolfk) VALUES('A000007','Miguel','Gil','mail@','abc1234','active',1);
INSERT INTO a_user (id_user,user_name,user_sname,user_mail,user_pwd,user_status,id_rolfk) VALUES('A000008','Leo','Martinez','mail@','abc1234','active',1);

insert into a_rol(id_rol,rol_name) values(1,'admin');
insert into a_rol(id_rol,rol_name) values(2,'user');