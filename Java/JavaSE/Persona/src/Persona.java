import java.util.Scanner; 
/*
 Haz una clase llamada Persona que siga las siguientes condiciones: 
• Sus atributos son: nombre, edad, DNI, sexo (H hombre, M mujer), peso y altura. No queremos que se accedan
 directamente a ellos. Piensa que modificador de acceso es el más adecuado, también su tipo. Si quieres añadir 
 algún atributo puedes hacerlo.
 • Por defecto, todos los atributos menos el DNI serán valores por defecto según su tipo (0 números, cadena vacía para String, etc.). 
 Sexo será hombre por defecto, usa una constante para ello. 
 • Se implantaran varios constructores:  o Un constructor por defecto. 
 	o Un constructor con el nombre, edad y sexo, el resto por defecto. 
 	o Un constructor con todos los atributos como parámetro. 
 • Los métodos que se implementaran son:  o calcularIMC(): 
 calculara si la persona está en su peso ideal (peso en kg/(altura^2  en m)),
  si esta fórmula devuelve un valor menor que 20, la función devuelve un -1,
  si devuelve un número entre 20 y 25 (incluidos), significa que está por debajo de su peso ideal la 
  función devuelve un 0  y si devuelve un valor mayor que 25 significa que tiene sobrepeso, la función 
  devuelve un 1. Te recomiendo que uses constantes para devolver estos valores. 
  
    esMayorDeEdad(): 
  indica si es mayor de edad, devuelve un booleano.
  comprobarSexo(char sexo): comprueba que el sexo introducido 
  es correcto. Si no es correcto, sera H. No sera visible al exterior. 
  toString(): devuelve toda la información
   del objeto.
   
     generaDNI(): genera un número aleatorio de 8 cifras, genera a partir de este su número su letra 
   correspondiente. Este método sera invocado cuando se construya el objeto. Puedes dividir el método para que te 
le al exterior.  Métodos set de cada parámetro, excepto de DNI. 

Ahora, crea una clase ejecutable que haga lo siguiente: 
 • Pide por teclado el nombre, la edad, sexo, peso y altura. 
 
 • Crea 3 objetos de la clase anterior, el primer objeto obtendrá las anteriores variables pedidas por teclado,
 el segundo objeto obtendrá todos los anteriores menos el peso y la altura y el último por defecto, para 
 este último utiliza los métodos set para darle a los atributos un valor.
 • Para cada objeto, deberá comprobar
  si está en su peso ideal, tiene sobrepeso o por debajo de su peso ideal con un mensaje. 
 • Indicar para cada objeto si es mayor de edad. 
 • Por último, mostrar la información de cada objeto. 
Puedes usar métodos en la clase ejecutable, para que os sea más fácil.

 */

public class Persona {
	
	private String nombre;
	private int edad;
	private String dni;
	private char sexo;
	private static int IMC_IDEAL = 0;
	private static int IMC_DELGADO = -1;
	private static int IMC_SOBREPESO = 1;
	private double altura;
	private double peso;
	
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}
	
	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}
	

	public Persona() {
		
	}
	
	public Persona(String dni) {
		this.nombre = "";
		this.edad = 0;
		this.dni = dni;
		this.altura = 0.0;
		this.peso = 0.0;
		
	}
	//revisar: peta
	//no peta
	public Persona(String nombre, int edad, char sexo) {
	this.nombre = nombre;
	this.edad = edad;
	this.dni = dni;
	this.setSexo(sexo);
	this.altura = 0.0;
	this.peso = 0.0;
	
	}
	//revisar: peta
	public Persona(String nombre, int edad,  char sexo, double altura, double peso) {
	this.nombre = nombre;
	this.edad = edad;
	this.dni = generarDni();
	this.setSexo(sexo);
	this.altura = altura;
	this.peso = peso;
	
	}
	//Constructor para persona2
	public Persona(String nombre, int edad, String dni, char sexo) {
	this.nombre = nombre;
	this.edad = edad;
	this.dni = dni;
	this.setSexo(sexo);
	this.setAltura(1.5);
	this.setPeso (55);

	}
	
	
	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", edad=" + edad + ", dni=" + dni + ", sexo=" + sexo + ", altura=" + altura
				+ ", peso=" + peso + "]";
	}

	public int calcularImc() {
		//peso ideal (peso en kg/(altura^2  en m))
		double pesoCalc = getPeso();
		double alturaCalc = getAltura();
		double calculoImc = pesoCalc / Math.pow(2, alturaCalc);
		int valor =0;
		/*
		 si esta fórmula devuelve un valor menor que 20, la función devuelve un -1,
  		 si devuelve un número entre 20 y 25 (incluidos), significa que está por debajo de su peso ideal la 
  		 función devuelve un 0  y si devuelve un valor mayor que 25 significa que tiene sobrepeso, la función 
  		 devuelve un 1. Te recomiendo que uses constantes para devolver estos valores.
		 */
			if(calculoImc <20.0) {
				 valor = IMC_DELGADO;
		
			}
			if(calculoImc >20.0 && calculoImc <25.0) {
				 valor =  IMC_IDEAL;
			}
			
			if(calculoImc >25.0) {
				 valor = IMC_SOBREPESO = 1;
			}

			return valor;
			 
			
			
		}
	
	public boolean esMayorDeEdad(int edad) {	
		boolean valor = false;
			if(edad >= 18) {
				valor = true;
		}
		return valor;
	}
	
	public void validarEdad(boolean boolEdad) {
		//System.out.println("dentro del metodo validarEdad");	
		if(boolEdad) {
			System.out.println("la Persona es Mayor de edad");
		}else {
			System.out.println("la Persona es menor de edad");
		}
	}
	
	public void comprobarSexo(char sex) {
		if(sex == 'M' || sex == 'H') {//este funciona
			System.out.println("el sexo de la persona es " + sex);
		}else {
			this.setSexo('H');
			System.out.println("el sexo de la persona es " + this.getSexo());
		}
			
	}
	
	//revisar: peta
	//no peta
	public void calculosFinales() {
		int peso = calcularImc();
		//le paso el valor de la constante para acceder a un case
        switch (peso) {
            case -1:
                System.out.println("La persona esta en su peso ideal");
                break;
            case 0:
                System.out.println("La persona esta delgada");
                break;
            case +1:
                System.out.println("La persona tiene sobrepeso");
                break;
        }
		
		boolean edadComprobar = esMayorDeEdad(getEdad());
		validarEdad(edadComprobar);	
		comprobarSexo(getSexo());
		System.out.println(toString());
	}
	
	public String generarDni() {
		String dni = "";
		for(int i=0;i<8;i++) {
			int num = (int)(10 * Math.random());//genero el nunero del 0-9 random
			String numCadena= String.valueOf(num);//paso el numero de int a string
			dni +=numCadena;//concateno strings
			
		}	
		String[] arr = new String[] {"A","Z","Y","X"};//añado solo un par de letras
		String letra = arr[(int) (Math.random()*3)];//saco un dato del array al azar
		String dniCreado = dni +=letra;//concateno numeros con las letras
		System.out.println("dni aleatorio = "+dniCreado);
		return dniCreado;
	}
	
	public static void main(String[] args) {
		
		System.out.println("Introduce los datos de persona1");
		Scanner sc = new Scanner(System.in);
		System.out.println("Nombre");
		String nombre= sc.next();
		
		System.out.println("Edad");
		int edad= sc.nextInt();
			
		System.out.println("Sexo");
		String sexo= sc.next();
		
		System.out.println("Altura");
		double altura= sc.nextDouble();
		System.out.println("Peso");
		double peso= sc.nextDouble();
		
		Persona persona1 = new Persona("47368362T");
		//persona1 = new Persona("Borja", 36, 'M');
		char sexoChar = sexo.charAt(0);
		persona1 = new Persona(nombre, edad, sexoChar, altura, peso);
		
		Persona persona2 = new Persona();
		persona2 = new Persona("Ana", 36, "47567435T", 'M');

		
		Persona persona3 = new Persona();
		persona3.setNombre("Juan");
		persona3.setEdad(18);
		persona3.setDni("12345675Z");
		persona3.setSexo('M');
		persona3.setAltura(1.8);
		persona3.setPeso(110);
		
/*
 • Crea 3 objetos de la clase anterior, el primer objeto obtendrá las anteriores variables pedidas por teclado,
   el segundo objeto obtendrá todos los anteriores menos el peso y la altura y el último por defecto, para 
   este último utiliza los métodos set para darle a los atributos un valor.
 • Para cada objeto, deberá comprobar
   si está en su peso ideal, tiene sobrepeso o por debajo de su peso ideal con un mensaje. 
 • Indicar para cada objeto si es mayor de edad. 
 • Por último, mostrar la información de cada objeto. 
  Puedes usar métodos en la clase ejecutable, para que os sea más fácil.
*/
		
		//Persona1
		System.out.println("---------------Persona 1");
		persona1.calculosFinales();	
		
		//Persona2
		System.out.println("---------------Persona 2");
		persona2.calculosFinales();	
		//Persona3
		System.out.println("---------------Persona 3");
		persona3.calculosFinales();	
		
		persona2.generarDni();
		
	}


}


