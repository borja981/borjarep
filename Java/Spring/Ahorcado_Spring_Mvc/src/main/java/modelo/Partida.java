package modelo;

public class Partida {
	
	public String palabra;
	public int vidas;
	
	public Partida() {
		
	}
	
	public Partida(String palabra) {
		super();
		this.palabra = palabra;
	}
	
	public Partida(String palabra, int vidas) {
		super();
		this.palabra = palabra;
		this.vidas = vidas;
	}

	public String getPalabra() {
		return palabra;
	}

	public void setPalabra(String palabra) {
		this.palabra = palabra;
	}

	public int getVidas() {
		return vidas;
	}

	public void setVidas(int vidas) {
		this.vidas = vidas;
	}
	
	
	
	

}
