package configuracion;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/*
 * Indicar que es clase de configuracion para Spring. La utilizamos
 * para toda la infraestructura Spring MVC incluyendo 
 * DispatcherServlet
 * 
 * Hay que implementar WebMvcConfigurer
 */
@Configuration
/*
 * Modulo MVC de Spring esta deshabilitado. Lo habilitamos
 */
@EnableWebMvc
/*
 * Habilitar el escaneo de componentes (@Controller) especificando 
 * paquete
 */
@ComponentScan(basePackages="atos.spring.mvc.web")
public class ComponentesWeb implements WebMvcConfigurer {

	/*
	 * Configuracion de recursos dentro de la aplicacion
	 * 
	 * Por defecto todos los recursos estaticos (CSS, Scripts, 
	 * imagenes,...) los sirve nuestro servlet.
	 * Con este metodo, todo lo que esta en la carpeta de recursos
	 * los sirve directamente el servidor de aplicaciones.
	 * 
	 * Nuestra App tiene mejor rendimiento
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/static/**").
			addResourceLocations("/resources/");
	}

	/*
	 * M�todos para creacion de componentes de Spring
	 * 
	 * Para configurar el ViewResolver para asociar nombre logico
	 * de las vistas con el recurso fisico (fichero) a cargar
	 * para generar la respuesta que se envia al cliente
	 */
	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver view = new
				InternalResourceViewResolver();
		
		/*
		 *  Prefijo para los identificadores/nombres logicos 
		 *  de las vistas
		 */
		view.setPrefix("/WEB-INF/views/");
		
		/*
		 *  Sufijo para los identificadores/nombres logicos 
		 *  de las vistas
		 */
		view.setSuffix(".jsp");
		
		/*
		 *  Tipo de vista (JSP con JSTL)
		 */
		view.setViewClass(JstlView.class);
		
		return view;

	}
	
}
