package configuracion;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/*
 * Indicar que es clase de configuracion para Spring
 */
@Configuration
/*
 * Modulo MVC de Spring esta deshabilitado. Lo habilitamos
 */
@EnableWebMvc
/*
 * Habilitar el escaneo de componentes (@Component, @Repository y
 * @Service) especificando paquete
 */
@ComponentScan(basePackages="atos.spring.mvc.web")
public class ComponentesNoWeb {

}
