package configuracion;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/*
 * Clase para configuracion del entorno MVC de Spring incluyendo
 * DispatcherServlet (equivalente al fichero web.xml)
 */
public class InicializadorApp extends AbstractAnnotationConfigDispatcherServletInitializer {

	/*
	 * Devolucion de todas las clases de configuracion de Spring para componentes
	 * generales de la aplicacion (componentes no web)
	 */
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class<?>[] { ComponentesNoWeb.class };
	}

	/*
	 * Devolucion de todas las clases de configuracion de Spring para componentes
	 * web de la aplicacion (@Controller)
	 */
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[] { ComponentesWeb.class };
	}

	/*
	 * Devolucion de todas los patrones de llamada asociados a DispatcherServlet
	 * 
	 * Cualquier peticion que se haga dentro de la aplicacion pasa
	 * por nuestro Servlet
	 */
	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

}
