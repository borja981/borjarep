package atos.spring.mvc.web;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import modelo.Partida;
import servicios.Servicio;

@Controller
public class PartidaController {
	
	private static final Logger logger = LoggerFactory.getLogger(PartidaController.class);
	
	Partida partida;
	
	 @RequestMapping(value = "/", method = RequestMethod.GET)
	    public String home(Locale locale, Model model) {
	        logger.info("Welcome home! the client locale is "+ locale.toString());
	        System.out.println("en /");
	        model.addAttribute("partida", new Partida());
	        
	        return "juego";
	    }
	 
	@RequestMapping(value = "/crearPartida.html", method = RequestMethod.POST)
	public ModelAndView alta(@ModelAttribute("partida") Partida partidas) {
		
		System.out.println(partidas.getPalabra());
		
		partidas.setVidas(5);	
		partida = partidas; //Guarrada
		
		return new ModelAndView("letra","partida",new Partida());
		
	}
	
	@RequestMapping(value = "/letra.html", method = RequestMethod.POST)
	public ModelAndView change(@RequestParam("letra") String letra) {
		
		System.out.println("letra: "+letra);
		
		Servicio servicio = new Servicio();
		boolean comprobarLetra = servicio.comprobar(partida, letra);
		
		return new ModelAndView("letra");
		
	}
	

}
