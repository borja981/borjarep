<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%-- Libreria core de JSTL --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%-- Libreria para formularios HTML de Spring MVC --%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Spring MVC</title>
</head>
<body>
	<h3>Listado Alumnos</h3>
	<br />
	<br />

	<%-- Comprobar si hay alumnos en la lista --%>
	<c:if test="${ !empty listado }">
		<table style="width: 500px; border: 1">
			<tr>
				<th>Nombre</th>
				<th>Email</th>
				<th>Telefono</th>
			</tr>

			<%-- Iterar por la lista (atributo listado) y guardar
				 objeto en variable alumno --%>
			<c:forEach items="${listado}" var="alumno">
				<tr>
					<th><c:out value="${alumno.nombre}" /></th>
					<th><c:out value="${alumno.email}" /></th>
					<th><c:out value="${alumno.telefono}" /></th>
					<td>
					<a href="alumno/${alumno.nombre}.html">Modificar alumno</a>
                    </td>
				</tr>
			</c:forEach>
		</table>
	</c:if>

	<br />
	<br />
	
	<h3><a href="nuevoAlumno.html">Crear nuevo alumno</a></h3>
</body>
</html>