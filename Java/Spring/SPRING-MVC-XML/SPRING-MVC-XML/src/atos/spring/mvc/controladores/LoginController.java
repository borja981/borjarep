package atos.spring.mvc.controladores;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import atos.spring.mvc.modelo.AlumnoForm;
import atos.spring.mvc.modelo.UserForm;

/*
 * Configurar como componente de Spring.
 * 
 * Controlador MVC. Funcionalmente las acciones (clase Java) con
 * la logica de negocio
 */
@Controller
public class LoginController {

	/*
	 * Crear usuario para simular validacion
	 */
	private static UserForm userForm = new UserForm("admin", "admin");

	/*
	 * M�todo de negocio del controlador.
	 * 
	 * Atiende peticiones con URL concretas y metodo de envio HTTP especifico
	 */
	@RequestMapping(value="/iniciar.html", method=RequestMethod.GET)
	public ModelAndView inicial() {
		/*
		 * Devolver ModelAndView creado con constructor general
		 * de 3 argumentos:
		 * 
		 * Primero => nombre logica de la vista. Se utiliza el objeto
		 * ViewResolver configurado para ir a la vista (fichero fisico)
		 * para generar la respuesta que se envia al cliente
		 * 
		 * Segundo => nombre de atributo
		 * 
		 * Tercero => valor atributo
		 */
		return new ModelAndView("login", "usuario", new UserForm());
	}

	/*
	 * M�todo de negocio del controlador.
	 * 
	 * Atiende peticiones con URL concretas y metodo de envio HTTP especifico
	 * 
	 * Validar credenciales de usuario introducidas desde formulario de login.jsp
	 * 
	 * Enlazar el atributo del modelo (usuario) con el argumento del metodo
	 * 
	 * Devolver nombre logico de la vista
	 */
	@RequestMapping(value="/verificarLogin.html", 
			method=RequestMethod.POST)
	public ModelAndView validar(@ModelAttribute("usuario") UserForm user) {
		
		// Validar datos enviados
		if(user.getUser().isEmpty() || user.getKey().isEmpty()) {
			return new ModelAndView("login", "mensaje", 
					"Campos obligatorios");
		}
		
		if(user.getUser().equals(userForm.getUser()) &&
				user.getKey().equals(userForm.getKey())) {
			
			ModelAndView modelAndView = new
					ModelAndView("alumnos", "mensaje", "ok");
			
			// Agregar nuevos objetos (atributos) al modelo
			modelAndView.addObject("usuario", user);
			modelAndView.addObject("alumnoForm", new AlumnoForm());
			
			return modelAndView;
		}
		
		return new ModelAndView("login", "mensaje", 
				"Credenciales incorrectas");
	}
}
