package atos.spring.mvc.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import atos.spring.mvc.modelo.AlumnoForm;

public class AlumnoServiceImpl implements AlumnoService {
	
	public static AlumnoForm borrarAlumno;
	private static List<AlumnoForm> students = new ArrayList<>();

	@Override
	public void add(AlumnoForm alumno) {
		students.add(alumno);	
	}

	@Override
	public List<AlumnoForm> getAll() {
		return students;
	}

	public AlumnoForm findAlumno(String alumno) {
		AlumnoForm alumnoSeleccionado = null;
		for (AlumnoForm alumnos: students) {
			if(alumnos.getNombre().equals(alumno)) {
				borrarAlumno = alumnos;
				alumnoSeleccionado = alumnos;
			}
		}
		return alumnoSeleccionado;
	}
	
	public void del(){
		students.remove(borrarAlumno);	
	}
}
