package atos.spring.mvc.services;

import java.util.List;

import atos.spring.mvc.modelo.AlumnoForm;

/*
 * Encapsular trabajo con datos SIN DAO porque NO hay BBDD
 */
public interface AlumnoService {

	void add(AlumnoForm alumno);

	void del();
	
	AlumnoForm findAlumno(String alumno);
	
	List<AlumnoForm> getAll();
	
}
