package atos.spring.mvc.controladores;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import atos.spring.mvc.modelo.AlumnoForm;
import atos.spring.mvc.services.AlumnoService;
import atos.spring.mvc.services.AlumnoServiceImpl;

/*
 * Configurar como componente de Spring.
 * 
 * Controlador MVC. Funcionalmente las acciones (clase Java) con
 * la logica de negocio
 */
@Controller
public class AlumnoController {

	String update;
	
	@RequestMapping(value = "nuevoAlumno.html", method = RequestMethod.GET)
	public ModelAndView one() {

		return new ModelAndView("alumno", "alumnoForm", 
				new AlumnoForm());

	}

	@RequestMapping(value = "listadoAlumnos.html", method = RequestMethod.POST)
	public ModelAndView two(@ModelAttribute("alumnoForm") AlumnoForm alumno) {

		AlumnoService service = new AlumnoServiceImpl();

		service.add(alumno);

		return new ModelAndView("alumnos", "listado", service.getAll());
	}

	@RequestMapping(value = "alumno/{nombre}", method = RequestMethod.GET)
	public ModelAndView three(@PathVariable("nombre")String nombre) {

		System.out.println("estoy en el metodo de controller modificar");

		AlumnoService service = new AlumnoServiceImpl();

		AlumnoForm alumno = service.findAlumno(nombre);

		update = alumno.getNombre();
		
		 return new ModelAndView("formularioMod", "alumno", alumno);

	}
	
	@RequestMapping(value = "alumnoModificar.html", method = RequestMethod.POST)
	public ModelAndView four(@ModelAttribute("alumnoForm") AlumnoForm alumno) {
		
		AlumnoService service = new AlumnoServiceImpl();
		
		System.out.println("obejto alumno: "+alumno.getNombre());
		alumno.setNombre(alumno.getNombre());
		
		service.add(alumno);
		service.del();//lo borro de una manera muy cutre
		
		return new ModelAndView("alumnos", "listado", service.getAll());

	}

}

