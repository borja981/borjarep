package atos.spring.core.autowiring.nombre;

public class Cliente {

	private Direccion direccion2;

	public Direccion getDireccion2() {
		return direccion2;
	}

	public void setDireccion2(Direccion direccion) {
		this.direccion2 = direccion;
	}

	@Override
	public String toString() {
		return "Cliente [direccion=" + direccion2 + "]";
	}

}
