package atos.spring.core.ciclovida;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/*
 * Clase para ver diferentes opciones de creaci�n de objetos
 * y las dependencias del codigo
 * 
 * Ejecucion del metodo generarFichero de la clase FicheroCSV
 * 
 * Configuracion Spring contenida en diferentes ficheros. 
 * Prop�sito:
 * poder reutilizar ficheros de configuracion
 */
public class Test4 {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext(
						"META-INF/spring-all.xml");

		Cliente2 cliente2 =
				(Cliente2) context.getBean("cliente2");
		
		cliente2.setMensaje("Modificado!!!!");
		
		System.out.println(cliente2);
		
		// Cerrar contexto. Liberamos memoria de las instancias de
		// los beans y los servicios utilizados
		context.close();
	}

}
