package atos.spring.core.herencia;

public class Alumno {

	private String rama;
	private String provincia;
	private int cpostal;

	public String getRama() {
		return rama;
	}

	public void setRama(String rama) {
		this.rama = rama;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public int getCpostal() {
		return cpostal;
	}

	public void setCpostal(int cpostal) {
		this.cpostal = cpostal;
	}

	@Override
	public String toString() {
		return "Alumno [rama=" + rama + ", provincia=" + provincia + ", cpostal=" + cpostal + "]";
	}

}
