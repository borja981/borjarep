package atos.spring.core.autowiring;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("motoBean")
public class Moto implements Vehiculo {

	@Override
	public void arrancar() {
		System.out.println("Moto arrancada");
	}

	@Override
	public void parar() {
		System.out.println("Moto parada");
	}

}
