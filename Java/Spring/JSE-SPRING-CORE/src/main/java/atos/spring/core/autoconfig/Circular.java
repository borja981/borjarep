package atos.spring.core.autoconfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Circular implements Movimiento {

	private Vehiculo vehiculo;
	
	/*
	 * Indicar al contenedor de Spring que inyecte un componente
	 * de tipo Vehiculo al instanciar el componente Circular
	 */
	@Autowired
	public Circular(Vehiculo vehiculo) {
		this.vehiculo = vehiculo;
	}

	@Override
	public void mover() {
		vehiculo.marcha();
	}

}
