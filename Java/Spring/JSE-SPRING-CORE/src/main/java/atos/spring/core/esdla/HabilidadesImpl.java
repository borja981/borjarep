package atos.spring.core.esdla;


public class HabilidadesImpl implements Habilidades{


	public Personaje atacar(Personaje personaje) {
		int damage;
		if(personaje.getNombre().equals("Paladin")) {
			damage=10;

			System.out.println(personaje.getNombre() +" hizo un da�o de " +damage+" puntos usando la habilidad: " + personaje.getMagia());
		}
		if(personaje.getNombre().equals("Thorin")) {
			damage=15;
			System.out.println(personaje.getNombre() +" hizo un da�o de " +damage+" puntos");
		}
		if(personaje.getNombre().equals("legolas")) {
			damage=5;
			System.out.println(personaje.getNombre() +" hizo un da�o de " +damage+" puntos");
		}
		if(personaje.getNombre().equals("OrcoJuan")) {
			damage=20;
			System.out.println(personaje.getNombre() +" hizo un da�o de " +damage+" puntos");
		}

		return personaje;

	}
	public Personaje bloquear(Personaje personaje) {

		System.out.println("\n"+personaje.getNombre()+" ha bloqueado un golpe");
		return personaje;
	}

}
