package atos.spring.core.ciclovida;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/*
 * Clase para ver diferentes opciones de creaci�n de objetos
 * y las dependencias del codigo
 * 
 * Ejecucion del metodo generarFichero de la clase FicheroCSV
 * 
 */
public class Test {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext(
						"spring-ciclovida.xml");

		Cliente2 cliente2 =
				(Cliente2) context.getBean("cliente2");
		
		cliente2.setMensaje("Modificado!!!!");
		
		System.out.println(cliente2);
		
		//Estos metodos est�n creados por Borja
		Cliente3 cliente3 =
				(Cliente3) context.getBean("cliente3");
		
		// Cerrar contexto. Liberamos memoria de las instancias de
		// los beans y los servicios utilizados
		context.close();
	}

}
