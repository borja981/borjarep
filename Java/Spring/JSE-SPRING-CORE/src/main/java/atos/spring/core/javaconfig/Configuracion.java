package atos.spring.core.javaconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*
 * Clase utilizada como parte de la configuracion utilizada
 * por Spring
 * 
 * Indicamos con @Configuration
 * 
 * Contiene, entre otras cosas, metodos para instanciar los
 * beans por parte del contenedor. Esos metodos llevan la
 * anotacion @Bean
 * 
 */
@Configuration
public class Configuracion {

	/*
	 * Metodo para devolver un bean de tipo Vehiculo
	 */
	@Bean
	public Vehiculo xxxx() {
		return new Seat();
	}
	
	/*
	 * Metodo para devolver un bean de tipo Movimiento
	 */
	@Bean
	public Movimiento yyyy() {
		return new Circular();
	}
}
