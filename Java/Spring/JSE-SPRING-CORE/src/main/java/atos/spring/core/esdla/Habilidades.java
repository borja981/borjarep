package atos.spring.core.esdla;

public interface Habilidades {
		
		Personaje atacar(Personaje personaje);
		Personaje bloquear(Personaje personaje);
}
