package atos.spring.core.javaconfig;

import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Test {

	public static void main(String[] args) {
		
		// Cargar fichero de configuracion de Spring con los beans
		// Depende del tipo de aplicacion. Para JSE si esta situado
		// en la carpetan resources (como este caso)
		// ClassPathXmlApplicationContext
		ClassPathXmlApplicationContext context = new
				ClassPathXmlApplicationContext("spring-javaconfig.xml");
		
		// Recuperar bean por clase porque NO hemos configurado
		// ning�n id
		Movimiento movimiento = (Movimiento)
				context.getBean(Circular.class);
		
		movimiento.mover();
		
		// Cerrar contexto. Liberamos memoria de las instancias de
		// los beans y los servicios utilizados
		context.close();
	}

}
