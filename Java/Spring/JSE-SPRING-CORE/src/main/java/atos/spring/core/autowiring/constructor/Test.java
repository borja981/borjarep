package atos.spring.core.autowiring.constructor;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new 
				ClassPathXmlApplicationContext("spring-autowiring.xml");

		System.out.println(context.getBean("programador"));

		// Cerrar contexto. Liberamos memoria de las instancias de
		// los beans y los servicios utilizados
		context.close();
	}

}
