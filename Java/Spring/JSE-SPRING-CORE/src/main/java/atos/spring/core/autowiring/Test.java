package atos.spring.core.autowiring;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/*
 * Aplicacion de Spring donde todo el codigo esta configurado
 * mediante anotaciones (como este caso) NO hace falta tener
 * fichero/s de configuracion
 * 
 * Para cargar Spring utilizar la clase 
 * AnnotationConfigApplicationContext
 * 
 */
public class Test {

	public static void main(String[] args) {

		AnnotationConfigApplicationContext context = 
				new AnnotationConfigApplicationContext();

		// Escanear (buscar) todos los beans (componentes) de la
		// aplicacion. Equivalente a @ComponentScan o 
		// <context:component-scan
		// context.scan("atos.spring.core.*");
		context.scan("atos.spring.core.autowiring");
		
		// Actualizar contexto para que coja las modificaiones
		context.refresh();
		
		// Recuperar vehiculo por tipo de datos (clase)
		ServicioVehiculos service = 
				context.getBean(ServicioVehiculos.class);
		
		service.servicio();
		
		// Cerrar contexto. Liberamos memoria de las instancias de
		// los beans y los servicios utilizados
		context.close();
	}

}
