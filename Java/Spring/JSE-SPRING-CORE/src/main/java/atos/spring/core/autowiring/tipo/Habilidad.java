package atos.spring.core.autowiring.tipo;

/*
 * Autowiring => Inyeccion de valores (simples u objetos) en
 * campos/propiedades de un bean
 * 
 * Anotación @Autowired (campo, set o constructor)
 * 
 * EN XML PODEMOS CONFIGURARLO CON EL ATRIBUTO autowire del
 * elemento bean. Posibles valores:
 * 
 * none => Valor por defecto. NO hay autowiring. Establecemos
 * los valores con value (literales) o ref (objetos).
 * 
 * byName => Inyectamos por coincidencia de nombre/id (equivalente
 * a la anotación @Qualifier)
 * 
 * byType => Inyectamos por coincidencia de tipo de dato
 * 
 * constructor => Inyectamos por coincidencia de tipo de datos de
 * los argumentos del constructor
 * 
 * default => Inyectamos por constructor y si no hay coincidencias
 * por tipo
 */
public class Habilidad {

	private String skill;

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	@Override
	public String toString() {
		return "Habilidad [skill=" + skill + "]";
	}

}
