package atos.spring.core.ciclovida;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/*
 * Interfaz InitializingBean => metodo afterPropertiesSet
 * 
 * Se ejecuta una vez se ha creado y se han establecido las
 * propiedades del bean.
 * 
 * Interfaz DisposableBean => metodo destroy
 * 
 * Se ejecuta justo antes de que se elimine la instancia
 * 
 */
public class Cliente implements InitializingBean, DisposableBean {

	private String mensaje;

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	// DisposableBean
	@Override
	public void destroy() throws Exception {
		System.out.println("Metodo finalizacion bean Cliente\n" + "Antes de cerrar context (Aplicacion Spring)");
	}

	// InitializingBean
	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("Metodo inicializacion bean Cliente\n" + "Propiedades establecidas\nMensaje: " + mensaje);
	}

}
