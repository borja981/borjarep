package atos.spring.core.ciclovida;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/*
 * Inicializacion y finalizacion del bean mediante las antocaciones
 * @PostConstruct y @PreDestroy como alternativa a los atributos
 * init-method y destroy-method del fichero de configuracion y a
 * las interfaces InitializingBean y DisposableBean
 * 
 * Las anotaciones @PostConstruct y @PreDestroy NO SON de Spring.
 * Son de JEE (paquete javax.annotation)
 * 
 * Por defecto estas anotaciones est�n deshabilitadas. Hay que
 * habilitarlas de cualquiera de las siguientes formas:
 * 
 * 1.- Crear un bean de tipo CommonAnnotationBeanPostProcesor
 * 
 * 2.- Habilitarlas con el elemento annotation-config del esquema
 * context de Spring
 * 
 */
public class Cliente3 {

	private String mensaje;

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	@PostConstruct
	public void xxxxx() throws Exception {
		System.out.println("Metodo inicializacion bean Cliente3\n" 
			+ "Propiedades establecidas\nMensaje: " + mensaje);
	}
	
	@PreDestroy
	public void yyyyy() throws Exception {
		System.out.println("Metodo finalizacion bean Cliente3\n" 
			+ "Antes de cerrar context (Aplicacion Spring)");
	}
}
