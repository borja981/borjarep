package atos.spring.core.autowiring.nombre;

public class Direccion {

	private String direccionCompleta;

	public String getDireccionCompleta() {
		return direccionCompleta;
	}

	public void setDireccionCompleta(String direccionCompleta) {
		this.direccionCompleta = direccionCompleta;
	}

	@Override
	public String toString() {
		return "Direccion [direccionCompleta=" + direccionCompleta + "]";
	}

}
