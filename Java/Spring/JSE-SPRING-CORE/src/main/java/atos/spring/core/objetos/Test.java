package atos.spring.core.objetos;

import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Test {

	public static void main(String[] args) {
		
		// Cargar fichero de configuracion de Spring con los beans
		// Depende del tipo de aplicacion. Para JSE si esta situado
		// en la carpetan resources (como este caso)
		// ClassPathXmlApplicationContext
		ClassPathXmlApplicationContext context = new
				ClassPathXmlApplicationContext("spring-objetos.xml");
				
		System.out.println(context.getBean("user1"));
		System.out.println(context.getBean("user2"));
		System.out.println(context.getBean("user3"));
		System.out.println(context.getBean("user4"));
		
		
		// Cerrar contexto. Liberamos memoria de las instancias de
		// los beans y los servicios utilizados
		context.close();
	}

}
