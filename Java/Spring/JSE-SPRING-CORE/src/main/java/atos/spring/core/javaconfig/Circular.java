package atos.spring.core.javaconfig;

import org.springframework.beans.factory.annotation.Autowired;

public class Circular implements Movimiento {

	private Vehiculo vehiculo;

	/*
	 * Indicar al contenedor de Spring que inyecte un componente de tipo Vehiculo al
	 * poner valor de la propiedad
	 */
	@Autowired
	public void setVehiculo(Vehiculo vehiculo) {
		this.vehiculo = vehiculo;
	}

	@Override
	public void mover() {
		vehiculo.marcha();
	}

}
