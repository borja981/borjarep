package atos.spring.core.esdla;

import org.aspectj.lang.annotation.*;

public class PersonajeBefore {
	
	@Before("execution(* getNombre())")
	public void mostrarArgumentos(/*String name*/) {
		System.out.println("\nEjecutandoBefore");
	}

}
