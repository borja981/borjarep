package atos.spring.core.javaconfig;

public class Seat implements Vehiculo {

	private String marca = "Seat";
	private String modelo = "Leon FX";
	private int anio = 2017;

	@Override
	public void marcha() {
		System.out.println("\nCoche " + marca + " " + modelo + " del a�o " + anio + " en marcha");
	}

}
