package atos.spring.core.previo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/*
 * Clase para ver diferentes opciones de creación de objetos
 * y las dependencias del codigo
 * 
 * Ejecucion del metodo generarFichero de la clase FicheroCSV
 * 
 */
public class Test {

	public static void main(String[] args) {
		/*
		 * Ejecutar metodo de negocio de un componente Java
		 * 
		 * Primera opcion. Instanciar objeto de la clase
		 * 
		 * CODIGO FUERTEMENTE ACOPLADO. CADA MODIFICACION QUE HAYA
		 * QUE HACER, CAMBIA EL CODIGO JAVA
		 */
		FicheroCSV csv = new FicheroCSV();
		
		csv.generarFichero();
		
		GeneradorFicheros generador = new FicheroCSV();
		
		generador.generarFichero();

		/* Segunda opcion. Utilizar un Helper (clase que lleva
		 * toda la implementación
		 * 
		 * CODIGO ACOPLADO. CADA MODIFICACION QUE HAYA
		 * QUE HACER, CAMBIA EL CODIGO JAVA
		 */
		GeneradorFicherosHelper helper = new GeneradorFicherosHelper(
				new FicheroCSV());
		
		helper.generarFichero();
		
		
		/* Tercera opcion. Utilizar Spring
		 * 
		 * CODIGO NO ACOPLADO. CADA MODIFICACION QUE HAYA
		 * QUE HACER NO CAMBIA EL CODIGO JAVA. SOLO HAY QUE
		 * CAMBIAR FICHEROS DE CONFICURACION (XML)
		 */
		
		// Cargar fichero de configuracion de Spring con los beans
		// Depende del tipo de aplicacion. Para JSE si esta situado
		// en la carpetan resources (como este caso)
		// ClassPathXmlApplicationContext
		ClassPathXmlApplicationContext context = new
				ClassPathXmlApplicationContext("spring-beans.xml");
		
		
		// Recuperar bean por su id mediante metodo getBean. Como
		// devuelve Object convertimos a la clase del bean
		GeneradorFicherosHelper helper2 =
				(GeneradorFicherosHelper) context.getBean("generador");
		
		helper2.generarFichero();
		
		/*
		GeneradorFicheros json =
				(GeneradorFicheros) context.getBean("json");
		
		json.generarFichero();
		
		GeneradorFicheros csv2 =
				(GeneradorFicheros) context.getBean("csv");
		
		csv2.generarFichero();
		*/
		
		// Cerrar contexto. Liberamos memoria de las instancias de
		// los beans y los servicios utilizados
		context.close();
	}

}
