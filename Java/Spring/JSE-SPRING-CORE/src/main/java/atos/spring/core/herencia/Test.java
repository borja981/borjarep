package atos.spring.core.herencia;

import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Test {

	public static void main(String[] args) {
		
		// Cargar fichero de configuracion de Spring con los beans
		// Depende del tipo de aplicacion. Para JSE si esta situado
		// en la carpetan resources (como este caso)
		// ClassPathXmlApplicationContext
		ClassPathXmlApplicationContext context = new
				ClassPathXmlApplicationContext("spring-herencia.xml");
		
		System.out.println(context.getBean("alumnoMadrid"));
		System.out.println(context.getBean("alumnoSegovia"));
		System.out.println(context.getBean("alumnoToledo"));
		System.out.println(context.getBean("alumnoBorja"));
		
		// No se puede instanciar bean abstracto
		// System.out.println(context.getBean("baseAlumno"));
		
		// Cerrar contexto. Liberamos memoria de las instancias de
		// los beans y los servicios utilizados
		context.close();
	}

}
