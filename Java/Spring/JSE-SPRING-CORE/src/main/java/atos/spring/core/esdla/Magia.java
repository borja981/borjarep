package atos.spring.core.esdla;

public class Magia {
	
	public String magiaTipo;
	public int magiaDaņo;
	
	public Magia() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getMagiaTipo() {
		return magiaTipo;
	}

	public void setMagiaTipo(String magiaTipo) {
		this.magiaTipo = magiaTipo;
	}

	public int getMagiaDaņo() {
		return magiaDaņo;
	}

	public void setMagiaDaņo(int magiaDaņo) {
		this.magiaDaņo = magiaDaņo;
	}

	@Override
	public String toString() {
		return "Magia [magiaTipo=" + magiaTipo + ", magiaDaņo=" + magiaDaņo + "]";
	}
	
	
	
	
}

