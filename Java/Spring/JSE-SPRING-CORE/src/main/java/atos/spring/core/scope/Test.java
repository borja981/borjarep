package atos.spring.core.scope;

import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Test {

	public static void main(String[] args) {
		
		// Cargar fichero de configuracion de Spring con los beans
		// Depende del tipo de aplicacion. Para JSE si esta situado
		// en la carpetan resources (como este caso)
		// ClassPathXmlApplicationContext
		ClassPathXmlApplicationContext context = new
				ClassPathXmlApplicationContext("spring-scope.xml");
		
		Cliente cliente = (Cliente)
				context.getBean("cliente");
		
		cliente.setMensaje("Ambito de un bean");
		
		System.out.println("Bean cliente: " + cliente.getMensaje());
		
		Cliente cliente2 = (Cliente)
				context.getBean("cliente");
		
		System.out.println("Bean cliente2: " + cliente2.getMensaje());
		
		// Cerrar contexto. Liberamos memoria de las instancias de
		// los beans y los servicios utilizados
		context.close();
	}

}
