package atos.spring.core.autowiring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class ServicioVehiculos {

	/*
	 * Inyectar componente en el campo (alternativa a
	 * constructor y metodo set de la propiedad)
	 */
	@Autowired
	@Qualifier("carBean")
	private Vehiculo vehiculo;
	
	public void servicio() {
		vehiculo.arrancar();
		vehiculo.parar();
	}

}
