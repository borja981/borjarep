package atos.spring.core.previo;

/*
 * Clase de ayuda para intentar quitar el acoplamiento (dependencias)
 * fuerte del codigo
 */
public class GeneradorFicherosHelper {

	private GeneradorFicheros generadorFicheros;

	public GeneradorFicherosHelper() {

	}

	public GeneradorFicherosHelper(GeneradorFicheros generadorFicheros) {
		this.generadorFicheros = generadorFicheros;
	}

	public GeneradorFicheros getGeneradorFicheros() {
		return generadorFicheros;
	}

	public void setGeneradorFicheros(GeneradorFicheros generadorFicheros) {
		this.generadorFicheros = generadorFicheros;
	}

	public void generarFichero() {
		generadorFicheros.generarFichero();
	}

}
