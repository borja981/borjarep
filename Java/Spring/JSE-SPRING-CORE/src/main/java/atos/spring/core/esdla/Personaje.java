package atos.spring.core.esdla;

public class Personaje {
	
	private String nombre;
	private String arma;
	private String pelicula;
	private String a�o;
	private Magia magia;
	
	public Personaje() {
		
	}
	
	public Personaje(String nombre, String arma, String pelicula, String a�o, Magia magia) {
		super();
		this.nombre = nombre;
		this.arma = arma;
		this.pelicula = pelicula;
		this.a�o = a�o;
		this.magia = magia;
	}
	
	public Personaje(String nombre, String arma, String pelicula, String a�o) {
		super();
		this.nombre = nombre;
		this.arma = arma;
		this.pelicula = pelicula;
		this.a�o = a�o;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getArma() {
		return arma;
	}
	public void setArma(String arma) {
		this.arma = arma;
	}
	public String getPelicula() {
		return pelicula;
	}
	public void setPelicula(String pelicula) {
		this.pelicula = pelicula;
	}
	public String getA�o() {
		return a�o;
	}
	public void setA�o(String a�o) {
		this.a�o = a�o;
	}
	

	public Magia getMagia() {
		return magia;
	}

	public void setMagia(Magia magia) {
		this.magia = magia;
	}

	@Override
	public String toString() {
		return "Personaje [nombre=" + nombre + ", arma=" + arma + ", pelicula=" + pelicula + ", a�o=" + a�o + ", magia="
				+ magia + "]";
	}
	
}
