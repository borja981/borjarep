package atos.spring.core.ciclovida;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/*
 * Clase para ver diferentes opciones de creaci�n de objetos
 * y las dependencias del codigo
 * 
 * Ejecucion del metodo generarFichero de la clase FicheroCSV
 * 
 */
public class Test2 {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext(
						"spring-ciclovida2.xml");

		
		// Cerrar contexto. Liberamos memoria de las instancias de
		// los beans y los servicios utilizados
		context.close();
	}

}
