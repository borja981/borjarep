package atos.spring.core.ciclovida;

/*
 * Inicializacion del bean mediante metodo asociado a atributo
 * init-method del fichero de configuracion
 * 
 * Finalizacion del bean mediante metodo asociado a atributo
 * destroy-method del fichero de configuracion
 * 
 */
public class Cliente2 {

	private String mensaje;

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public void init() throws Exception {
		System.out.println("Metodo inicializacion bean Cliente2\n" 
			+ "Propiedades establecidas\nMensaje: " + mensaje);
	}
	
	public void remove() throws Exception {
		System.out.println("Metodo finalizacion bean Cliente2\n" 
			+ "Antes de cerrar context (Aplicacion Spring)");
	}
}
