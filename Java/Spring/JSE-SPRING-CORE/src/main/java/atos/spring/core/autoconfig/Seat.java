package atos.spring.core.autoconfig;

import org.springframework.stereotype.Component;

/*
 * Autoconfiguración de clase Java como componente o bean 
 * de Spring
 * 
 * Alternativa a los ficheros de configuracion
 * 
 * @Component
 */
@Component
public class Seat implements Vehiculo {

	private String marca = "Seat";
	private String modelo = "Leon FX";
	private int anio = 2017;

	@Override
	public void marcha() {
		System.out.println("\nCoche " + marca + " " + modelo + " del año " + anio + " en marcha");
	}

}
