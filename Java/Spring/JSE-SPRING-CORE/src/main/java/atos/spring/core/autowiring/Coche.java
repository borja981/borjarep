package atos.spring.core.autowiring;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/*
 * @Qualifier => Registrar componente cuyo id/nombre se especifica
 */
@Component
@Qualifier("carBean")
public class Coche implements Vehiculo {

	@Override
	public void arrancar() {
		System.out.println("Coche arrancado");
	}

	@Override
	public void parar() {
		System.out.println("Coche parado");
	}

}
