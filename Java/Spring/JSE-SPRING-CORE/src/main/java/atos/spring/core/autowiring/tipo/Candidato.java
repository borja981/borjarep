package atos.spring.core.autowiring.tipo;

public class Candidato {

	private Habilidad habilidad;

	public Habilidad getHabilidad() {
		return habilidad;
	}

	public void setHabilidad(Habilidad habilidad) {
		this.habilidad = habilidad;
	}

	@Override
	public String toString() {
		return "Candidato [habilidad=" + habilidad + "]";
	}

}
