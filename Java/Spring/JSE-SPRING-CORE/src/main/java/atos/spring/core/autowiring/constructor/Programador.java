package atos.spring.core.autowiring.constructor;

public class Programador {

	private Lenguaje lenguaje;

	public Programador(Lenguaje lenguaje) {
		this.lenguaje = lenguaje;
	}

	@Override
	public String toString() {
		return "Programador [lenguaje=" + lenguaje + "]";
	}

}
