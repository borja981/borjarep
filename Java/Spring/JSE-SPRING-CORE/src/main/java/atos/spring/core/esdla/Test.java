package atos.spring.core.esdla;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import atos.spring.core.autoconfig.Circular;
import atos.spring.core.autoconfig.Movimiento;

public class Test {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new
				ClassPathXmlApplicationContext("spring-esdla.xml");
		
		System.out.println(context.getBean("elfo"));//elfo magia con autowire
		System.out.println(context.getBean("humano"));//Humano magia con autowire
		System.out.println(context.getBean("enano"));
		System.out.println(context.getBean("orco"));
		
		//Magias////////////////////////////////////////////////////////////
		//Para setear en la magia de humano
		/*
		System.out.println(context.getBean("magiaHumana"));
		Magia magiaHumana = (Magia)context.getBean("magiaHumana");
		*/
		
		/////////////////////////////////////////////////////////////////////
		//Rey singleton
		System.out.println(context.getBean("rey"));
		Personaje rey = (Personaje) context.getBean("rey");
		System.out.println("\nEl "+rey.getNombre()+" posee el arma : "+rey.getArma());
		rey.getArma(); 
		/////////////////////////////////////////////////////////////////////
		
		//Ataques
		System.out.println("\nAtaque humano");
		
		Personaje humano = (Personaje) context.getBean("humano");
		Habilidades habilidad = new HabilidadesImpl();
		
		habilidad.atacar(humano);
		
		System.out.println("\nAtaque elfo");
		Personaje elfo = (Personaje) context.getBean("elfo");
			
		//elfo con autowire
		//Personaje elfo = (Personaje) context.getBean("elfo");
		
		elfo = habilidad.atacar(elfo);
		humano = habilidad.bloquear(elfo);
		
		//Magias
		//System.out.println(context.getBean("magia"));
		
		context.close();
	}

}
