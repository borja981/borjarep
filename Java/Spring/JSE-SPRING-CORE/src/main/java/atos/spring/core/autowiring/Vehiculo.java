package atos.spring.core.autowiring;

public interface Vehiculo {

	void arrancar();
	
	void parar();
	
}
