package com.atos.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


/*
 * Entidad JPA para tabla HR.REGIONS
 * 
 */
@Entity
@Table(name="AA_REGIONS")//, schema="HR")
public class Region implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="REGION_ID")
	private int regionId;

	@Column(name="REGION_NAME")
	private String regionName;
	
	@OneToMany(mappedBy="region")
	private List<Country> countries;
	
	public Region(int regionId, String regionName, List<Country> countries) {
		super();
		this.regionId = regionId;
		this.regionName = regionName;
		this.countries = countries;
	}

	public Region() {
	}

	public int getRegionId() {
		return this.regionId;
	}

	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}

	public String getRegionName() {
		return this.regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	@Override
	public String toString() {
		return "\nRegion [regionId=" + regionId + ", regionName=" + regionName + "]";
	}
	
	

}