package com.atos.controlador;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.atos.modelo.Country;
import com.atos.modelo.Geographies;

@Repository
public class GeographiesDAOImpl implements GeographiesDAO {
	
	public GeographiesDAOImpl() {
	}
	
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public void create(Geographies geographies) {
		
		em.persist(geographies);
		System.out.println("\nGeografia a�adida");
		
	}
	@Override
	public void delete(int idgeographies) {
		em.remove(em.getReference(Geographies.class, idgeographies));
		System.out.println("\nGeografia eliminada");
	}
	
	

}
