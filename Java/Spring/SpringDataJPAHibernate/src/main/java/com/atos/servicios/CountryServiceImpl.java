package com.atos.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.atos.controlador.CountryDAO;
import com.atos.modelo.Country;

/*
 * Configurar componente de Spring mediante anotaciones.
 * 
 * Como esta en la capa de servicios @Service
 * 
 * Aqu� gestionamos las transacciones de los metodos de negocio
 */
@Service
public class CountryServiceImpl implements CountryService {

	/*
	 * Inyectar el dao en el servicio
	 */
	@Autowired
	private CountryDAO dao;

	/*
	 * Gestion de transacion por parte de String
	 */
	@Transactional
	@Override
	public void add(Country country) {
		dao.create(country);
	}

	/*
	 * Gestion de transacion por parte de String
	 */
	@Transactional
	@Override
	public void update(Country country) {
		dao.edit(country);
	}

	/*
	 * Gestion de transacion por parte de String
	 */
	@Transactional
	@Override
	public void destroy(String countryId) {
		dao.delete(countryId);
	}

	/*
	 * Gestion de transacion por parte de String.
	 * 
	 * Como es de consulta la marcamos de solo lectura
	 */
	@Transactional(readOnly = true)
	@Override
	public Country getById(String countryId) {
		return dao.findById(countryId);
	}

	/*
	 * Gestion de transacion por parte de String.
	 * 
	 * Como es de consulta la marcamos de solo lectura
	 */
	@Transactional(readOnly = true)
	@Override
	public List<Country> getAll() {
		return dao.findAll();
	}
	
	@Transactional(readOnly = true)
	@Override
	public List<Country> viewRegionId(int regionId) {
		return dao.viewRegionId(regionId);
	}
	

}
