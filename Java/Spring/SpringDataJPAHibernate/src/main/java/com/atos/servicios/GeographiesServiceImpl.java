package com.atos.servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.atos.controlador.GeographiesDAO;
import com.atos.modelo.Geographies;

@Service
public class GeographiesServiceImpl implements GeographiesService {
	
	@Autowired
	private GeographiesDAO dao;

	/*
	 * Gestion de transacion por parte de String
	 */
	@Transactional
	@Override
	public void add(Geographies geographies) {
		dao.create(geographies);
	}
	@Transactional
	@Override
	public void delete(int idgeographies) {
		dao.delete(idgeographies);
	}
	
	
}
