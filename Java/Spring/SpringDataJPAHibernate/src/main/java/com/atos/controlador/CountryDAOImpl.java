package com.atos.controlador;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.atos.modelo.Country;
import com.atos.modelo.Region;

/*
 * Configurar componente de Spring mediante anotaciones.
 * 
 * Como es el DAO configurar como repositorio
 */
@Repository
public class CountryDAOImpl implements CountryDAO {
	
	/*
	 * Configuración de EntityManager para unidad de persistencia
	 */
	@PersistenceContext
	private EntityManager em;

	@Override
	public void create(Country country) {
		em.persist(country);
	}

	@Override
	public void edit(Country country) {
		em.merge(country);
	}

	@Override
	public void delete(String countryId) {
		em.remove(em.getReference(Country.class, countryId));
	}

	@Override
	public Country findById(String countryId) {
		System.out.println("en findBy");
		return em.find(Country.class, countryId);
	}

	@Override
	public List <Country> viewRegionId(int regionId) {
		System.out.println("\ndentro del metodo viewRegionId");
		List <Country>lista;

		lista = em.createQuery("select b.country_name from Country b where region_id = "+regionId).getResultList();
				//("select b.country_name from Country b  where b.region_id = "+regionId).getResultList();

		return lista;
		}
	/*
	 * Podemos utilizar consulta JPQL estatica definida en
	 * la entidad @NamedQuery.... y crear con
	 * Query q = em.createNamedQuery
	 * 
	 * Podemos utilizar consulta JPQL dinamica definida y creada con
	 * Query q = em.createQuery
	 * 
	 * Podemos utilizar CriteriaQuery 
	 */
	@Override
	public List<Country> findAll() {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		
		CriteriaQuery<Country> query = 
				builder.createQuery(Country.class);
		
		// Clausula FROM consulta para recuperar datos 
		Root<Country> root = query.from(Country.class);
		
		// Hacer consulta de todos los campos ordenando por
		// campo regionId
		query.select(root).orderBy(builder.asc(root.get("country_id")));
		
		return em.createQuery(query).getResultList();
	}

}
