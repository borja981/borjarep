package com.atos.controlador;

import com.atos.modelo.Geographies;

public interface GeographiesDAO {
	
	void create(Geographies geographies);
	void delete(int idgeographies);

}
