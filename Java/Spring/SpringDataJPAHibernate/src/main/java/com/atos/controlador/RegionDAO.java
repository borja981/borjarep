package com.atos.controlador;

import java.util.List;

import com.atos.modelo.Region;

public interface RegionDAO {

	void create(Region region);

	void edit(Region region);

	void delete(int regionId);

	Region findById(int regionId);

	List<Region> findAll();
}
