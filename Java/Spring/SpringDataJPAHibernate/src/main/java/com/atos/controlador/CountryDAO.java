package com.atos.controlador;

import java.util.List;

import com.atos.modelo.Country;

public interface CountryDAO {
	
	void create(Country country);

	void edit(Country country);

	void delete(String countryId);

	Country findById(String countryId);

	List<Country> findAll();
	
	List<Country> viewRegionId(int regionId);
}

