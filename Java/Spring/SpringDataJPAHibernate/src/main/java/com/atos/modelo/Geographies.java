package com.atos.modelo;

import java.io.Serializable;
import javax.persistence.*;


@Entity
@Table(name="AA_GEOGRAPHIES")//, schema="HR")

public class Geographies implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="GEOGRAPHY_ID")
	private int geographyId;

	@Column(name="GEOGRAPHY_NAME")
	private String geographyName;

	@OneToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name="COUNTRY_ID")
	private Country country;
	
	public int getGeographyId() {
		return geographyId;
	}
	
	public void setGeographyId(int geographyId) {
		this.geographyId = geographyId;
	}

	public String getGeographyName() {
		return geographyName;
	}

	public void setGeographyName(String geographyName) {
		this.geographyName = geographyName;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Geographies() {
	}

	@Override
	public String toString() {
		return "Geography [geographyId=" + geographyId + "]";
	}
	
}
