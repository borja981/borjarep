package com.atos.ejecucion;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.atos.modelo.Country;
import com.atos.modelo.Geographies;
import com.atos.modelo.Region;
import com.atos.servicios.CountryService;
import com.atos.servicios.GeographiesService;
import com.atos.servicios.RegionService;

public class Test {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring-config.xml");


		////////////////////////////////////////////////////////////////////////////////////////////
		//Metodos Region
		RegionService servicio = (RegionService) ctx.getBean("regionServiceImpl");

		List<Region> regions = servicio.getAll();
		System.out.println("\nNumero regiones inicio: " + regions.size());
		System.out.println(regions);
		/*
		Region newRegion = new Region();
		newRegion.setRegionId(5);
		newRegion.setRegionName("Talavera");

		servicio.add(newRegion);
		System.out.println("\nRegion insertada");

		 */
		Region region2 = servicio.getById(2);
		//region2.setRegionName(region2.getRegionName() + "!");
		//servicio.update(region2);

		regions = servicio.getAll();
		System.out.println("\nNumero regiones actuales: " + regions.size());
		System.out.println(regions);

		System.out.println("estoy aqui");

		//servicio.destroy(5);

		regions = servicio.getAll();
		System.out.println("\nNumero regiones finales: " + regions.size());
		System.out.println(regions);

		////////////////////////////////////////////////////////////////////////////////////////////
		//Metodos country

		CountryService servicioCountry = (CountryService) ctx.getBean("countryServiceImpl");
		//mostrar todos
		List<Country> country = servicioCountry.getAll();
		System.out.println("\nNumero countries inicio: " + country.size());
		System.out.println(country.toString());
		/*
		//Crear country
		Country countryUno = new Country();
		countryUno  = servicioCountry.getById("IT");
		System.out.println(countryUno.getCountry_name());


		//Crear objeto
		Country newCountry = new Country();
		newCountry.setCountry_id("ZZ");
		newCountry.setCountry_name("coru�a");
		newCountry.setRegion(region2);//le paso objeto ya creado
		
		//Ver countries con region = 2
		List <Country> listaCountriesId;
		listaCountriesId = servicioCountry.viewRegionId(1);
		System.out.println(listaCountriesId);

		//Persitencia con el objto
		servicioCountry.add(newCountry);
		System.out.println("\nCountry insertada");
		*/

		//Metodos geografia	
		////////////////////////////////////////////////////////////////////////////////////////////
		GeographiesService geographiesService = (GeographiesService) ctx.getBean("geographiesServiceImpl");
	
		Geographies newGeographies = new Geographies();
		newGeographies.setGeographyId(1);
		newGeographies.setGeographyName("emisferio norte");
		newGeographies.setCountry(null);
		
		geographiesService.add(newGeographies);
		////////////////////////////////////////////////////////////////////////////////////////////

		//Borro �ltima country creada
		servicioCountry.destroy("ZZ");
		System.out.println("\nElemento eliminado");

		ctx.close();
	}

}
