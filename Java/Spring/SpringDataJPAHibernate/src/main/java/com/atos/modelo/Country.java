package com.atos.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
/*
@NamedQueries({
@NamedQuery(name="Country.viewRegions", query="Select e.country_name from countries e where e.region_id=2")
})
*/
@Table(name="AA_COUNTRIES")//, schema="HR")
public class Country {

	@Id
	@Column(name="COUNTRY_ID")
	private String country_id;
	
	@Column(name="COUNTRY_NAME")
	private String country_name;
	
	@ManyToOne
	@JoinColumn(name="region_id")
	private Region region;
	
	@OneToOne(mappedBy = "country")
	private Geographies geographies;

	public Country() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Country(String country_id, String country_name, Region region, Geographies geographies) {
		super();
		this.country_id = country_id;
		this.country_name = country_name;
		this.region = region;
		this.geographies = geographies;
	}



	public String getCountry_id() {
		return country_id;
	}

	public void setCountry_id(String country_id) {
		this.country_id = country_id;
	}

	public String getCountry_name() {
		return country_name;
	}

	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Geographies getGeographies() {
		return geographies;
	}

	public void setGeographies(Geographies geographies) {
		this.geographies = geographies;
	}

	@Override
	public String toString() {
		return "Country [country_id=" + country_id + ", country_name=" + country_name + ", region=" + region
				+ ", geographies=" + geographies + "]";
	}

	
}
