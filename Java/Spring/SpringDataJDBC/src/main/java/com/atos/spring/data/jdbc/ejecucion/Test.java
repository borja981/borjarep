package com.atos.spring.data.jdbc.ejecucion;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.atos.spring.data.jdbc.modelo.Country;
import com.atos.spring.data.jdbc.modelo.CountryDAO;
import com.atos.spring.data.jdbc.modelo.Region;
import com.atos.spring.data.jdbc.modelo.RegionDAO;

public class Test {

	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext ctx = new
				ClassPathXmlApplicationContext(
						"spring-configuration.xml");

		RegionDAO dao = 
				(RegionDAO) ctx.getBean("jdbcRegionDAO");
		
		Region newRegion = new Region(5, "Talavera");
		dao.insert(newRegion);
		
		Region region1 = dao.findById(1);
		System.out.println(region1);
		
		region1.setRegionName("Europe");
		dao.edit(region1);
		
		System.out.println(dao.findAll());
		
		dao.delete(5);
		
		//Operaciones Countries
		/////////////////////////////////////////////////
		CountryDAO countrydao = 
				(CountryDAO) ctx.getBean("jdbcCountryDAO");
		
		Country country1 = countrydao.findById("AR");
		System.out.println(country1.getCountry_name());
		
		//Paises por region
		System.out.println(countrydao.finByRegion(1));
		
		//Nuevo country
		Country country2 = new Country("ZZ","Coru�a",1);
		countrydao.insert(country2);
		
		//Borramos el creado
		countrydao.delete("ZZ");
		
		ctx.close();
	}

}
