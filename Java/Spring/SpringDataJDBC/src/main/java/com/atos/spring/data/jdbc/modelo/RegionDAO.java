package com.atos.spring.data.jdbc.modelo;

import java.util.List;

public interface RegionDAO {

	void insert(Region region);

	void edit(Region region);

	void delete(int regionId);

	Region findById(int regionId);

	List<Region> findAll();
	
	

}
