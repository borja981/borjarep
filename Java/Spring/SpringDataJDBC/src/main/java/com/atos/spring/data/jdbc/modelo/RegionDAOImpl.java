package com.atos.spring.data.jdbc.modelo;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

public class RegionDAOImpl implements RegionDAO {

	/*
	 * Origen de datos configurado con Spring a la BBDD
	 */
	private DataSource dataSource;

	/*
	 * Plantilla para automatizar codigo JDBC
	 */
	private JdbcTemplate jdbcTemplate;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public void insert(Region region) {
		String sql = "INSERT INTO HR.REGIONS(REGION_ID, REGION_NAME) " + "VALUES(?,?)";

		// Crear plantilla asociada a origen de datos
		jdbcTemplate = new JdbcTemplate(dataSource);

		// Ejecutar instruccion parametrizada
		// Primer argumento => instruccion SQL
		// Segundo argumento => matriz de Object con el valor
		// de los parametros de entrada
		jdbcTemplate.update(sql, new Object[] { region.getRegionId(), region.getRegionName() });
	}

	@Override
	public void edit(Region region) {
		String sql = "UPDATE HR.REGIONS SET REGION_NAME = ? " + "WHERE REGION_ID = ?";

		// Crear plantilla asociada a origen de datos
		jdbcTemplate = new JdbcTemplate(dataSource);

		// Ejecutar instruccion parametrizada
		// Primer argumento => instruccion SQL
		// Segundo argumento => matriz de Object con el valor
		// de los parametros de entrada
		jdbcTemplate.update(sql, new Object[] { region.getRegionName(), region.getRegionId() });
	}

	@Override
	public void delete(int regionId) {
		String sql = "DELETE FROM HR.REGIONS WHERE REGION_ID = ?";
		
		// Crear plantilla asociada a origen de datos
		jdbcTemplate = new JdbcTemplate(dataSource);

		// Ejecutar instruccion parametrizada
		// Primer argumento => instruccion SQL
		// Segundo argumento => matriz de Object con el valor
		// de los parametros de entrada
		jdbcTemplate.update(sql, new Object[] { regionId });
	}

	@Override
	public Region findById(int regionId) {
		String sql = "SELECT REGION_ID, REGION_NAME FROM "
				+ "HR.REGIONS WHERE REGION_ID = ?";

		// Crear plantilla asociada a origen de datos
		jdbcTemplate = new JdbcTemplate(dataSource);

		// Ejecutar instruccion parametrizada
		// Primer argumento => instruccion SQL
		// Segundo argumento => matriz de Object con el valor
		// de los parametros de entrada
		// Tercer argumento => BeanPropertyRowMapper. Enlaza las
		// columnas del SELECT con los campos del DTO o Entidad (JPA)
		// SIEMPRE QUE TENGAN LOS MISMOS NOMBRES
		Region region = 
				jdbcTemplate.queryForObject(sql, 
				new Object[] { regionId },
				new BeanPropertyRowMapper<>(Region.class));
		
		return region;
	}

	@Override
	public List<Region> findAll() {
		String sql = "SELECT REGION_ID, REGION_NAME FROM "
				+ "HR.REGIONS ORDER BY REGION_ID";

		// Crear plantilla asociada a origen de datos
		jdbcTemplate = new JdbcTemplate(dataSource);

		// Ejecutar instruccion parametrizada
		// Primer argumento => instruccion SQL
		// Segundo argumento => BeanPropertyRowMapper. Enlaza las
		// columnas del SELECT con los campos del DTO o Entidad (JPA)
		// SIEMPRE QUE TENGAN LOS MISMOS NOMBRES
		
		List<Region> regiones = 
				jdbcTemplate.query(sql,
				new BeanPropertyRowMapper<>(Region.class));
		
		return regiones;
	}

}
