package com.atos.spring.data.jdbc.modelo;

import java.util.List;

public interface CountryDAO {

	void insert(Country country);

	void edit(Country country);

	void delete(String countryId);

	Country findById(String countryId);

	List<Country> findAll();
	
	List<Country> finByRegion(int regionId);
}
