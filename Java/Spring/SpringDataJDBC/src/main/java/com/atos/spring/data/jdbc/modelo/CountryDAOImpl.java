package com.atos.spring.data.jdbc.modelo;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

public class CountryDAOImpl implements CountryDAO {
	

	/*
	 * Origen de datos configurado con Spring a la BBDD
	 */
	private DataSource dataSource;

	/*
	 * Plantilla para automatizar codigo JDBC
	 */
	private JdbcTemplate jdbcTemplate;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	
	public void insert(Country country) {
		String sql = "INSERT INTO HR.COUNTRIES(COUNTRY_ID, COUNTRY_NAME, REGION_ID) " + "VALUES(?,?,?)";

		// Crear plantilla asociada a origen de datos
		jdbcTemplate = new JdbcTemplate(dataSource);

		// Ejecutar instruccion parametrizada
		// Primer argumento => instruccion SQL
		// Segundo argumento => matriz de Object con el valor
		// de los parametros de entrada
		jdbcTemplate.update(sql, new Object[] { country.getCountry_id(), country.getCountry_name(), country.getRegion_id() });
	}

	
	public void edit(Country country) {
		String sql = "UPDATE HR.COUNTRIES SET COUNTRY_NAME = ? " + "WHERE COUNTRY_ID = ?";

		// Crear plantilla asociada a origen de datos
		jdbcTemplate = new JdbcTemplate(dataSource);

		// Ejecutar instruccion parametrizada
		// Primer argumento => instruccion SQL
		// Segundo argumento => matriz de Object con el valor
		// de los parametros de entrada
		jdbcTemplate.update(sql, new Object[] { country.getCountry_name(), country.getCountry_id() });
	}

	
	public void delete(String countryId) {
		String sql = "DELETE FROM HR.COUNTRIES WHERE COUNTRY_ID = ?";
		
		System.out.println("Se borra country con id: "+countryId);
		// Crear plantilla asociada a origen de datos
		jdbcTemplate = new JdbcTemplate(dataSource);

		// Ejecutar instruccion parametrizada
		// Primer argumento => instruccion SQL
		// Segundo argumento => matriz de Object con el valor
		// de los parametros de entrada
		jdbcTemplate.update(sql, new Object[] { countryId });
	}

	
	public Country findById(String countryId) {
		System.out.println("estoy en metodo findby");
		
		String sql = "SELECT COUNTRY_ID, COUNTRY_NAME, REGION_ID FROM "
				+ "HR.COUNTRIES WHERE COUNTRY_ID = ?";

		// Crear plantilla asociada a origen de datos
		jdbcTemplate = new JdbcTemplate(dataSource);

		// Ejecutar instruccion parametrizada
		// Primer argumento => instruccion SQL
		// Segundo argumento => matriz de Object con el valor
		// de los parametros de entrada
		// Tercer argumento => BeanPropertyRowMapper. Enlaza las
		// columnas del SELECT con los campos del DTO o Entidad (JPA)
		// SIEMPRE QUE TENGAN LOS MISMOS NOMBRES
		Country country = 
				jdbcTemplate.queryForObject(sql, 
				new Object[] { countryId },
				new BeanPropertyRowMapper<>(Country.class));
		
		return country;
	}

	@Override
	public List<Country> findAll() {
		String sql = "SELECT COUNTRY_ID, COUNTRY_NAME FROM "
				+ "HR.COUNTRIES ORDER BY COUNTRY_ID";

		// Crear plantilla asociada a origen de datos
		jdbcTemplate = new JdbcTemplate(dataSource);

		// Ejecutar instruccion parametrizada
		// Primer argumento => instruccion SQL
		// Segundo argumento => BeanPropertyRowMapper. Enlaza las
		// columnas del SELECT con los campos del DTO o Entidad (JPA)
		// SIEMPRE QUE TENGAN LOS MISMOS NOMBRES
		
		List<Country> countries = 
				jdbcTemplate.query(sql,
				new BeanPropertyRowMapper<>(Country.class));
		
		return countries;
	}
	
	public List<Country> finByRegion(int regionId) {
		String sql = "select country_name from countries where region_id = 2";

		// Crear plantilla asociada a origen de datos
		jdbcTemplate = new JdbcTemplate(dataSource);

		// Ejecutar instruccion parametrizada
		// Primer argumento => instruccion SQL
		// Segundo argumento => BeanPropertyRowMapper. Enlaza las
		// columnas del SELECT con los campos del DTO o Entidad (JPA)
		// SIEMPRE QUE TENGAN LOS MISMOS NOMBRES
		
		List<Country> countries = 
				jdbcTemplate.query(sql,
				new BeanPropertyRowMapper<>(Country.class));
		
		return countries;
	}

}
	


