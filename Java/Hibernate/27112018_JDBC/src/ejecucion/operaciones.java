package ejecucion;

import mvc.alquilerDAO;
import mvc.alquilerDAOImpl;
import mvc.alquilerDTO;
import mvc.autorDAO;
import mvc.autorDAOImpl;
import mvc.autorDTO;
import mvc.clienteDAO;
import mvc.clienteDAOImpl;
import mvc.clienteDTO;
import mvc.libroDAO;
import mvc.libroDAOImpl;
import mvc.libroDTO;
import mvc.portadaDAO;
import mvc.portadaDAOImpl;
import mvc.portadaDTO;
import servicios.alquilerService;
import servicios.alquilerServiceImpl;
import servicios.autorService;
import servicios.autorServiceImpl;
import servicios.clienteService;
import servicios.clienteServiceImpl;
import servicios.librosService;
import servicios.librosServiceImpl;
import servicios.portadaService;
import servicios.portadaServiceImpl;

public class operaciones {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
		//Autor -- libro
		/////////////////////////////////////////////////////
			autorDAO daoAutor =  new autorDAOImpl();
			autorService serviceAutor = new autorServiceImpl(daoAutor);
			
			libroDAO daoLibro = new libroDAOImpl();
			librosService serviceLibro = new librosServiceImpl(daoLibro);
				
			//Creamos un libro y un autor y lo imprimimos en pantalla
			serviceAutor.addAutorLibro((new autorDTO(11,"juan")),((new libroDTO(11,"libroUNO",7))));
			//System.out.println(serviceAutor.findById(1));
			serviceAutor.remove(11); //Primero creamos y despues borramos
			serviceLibro.remove(11); //Primero creamos y despues borramos
			
			//Ver todos los libros
			System.out.println("Ver todos los libros------------------");
			System.out.println(serviceLibro.findAll());
			
			//Borrar libro
			serviceLibro.remove(2);//Borrar libro
					
		
		//Alquiler			//
			alquilerDAO daoAlquiler = new alquilerDAOImpl();
			alquilerService serviceAlquiler = new alquilerServiceImpl(daoAlquiler);
			System.out.println("Ver los libros alquilados--------------");
			System.out.println(serviceAlquiler.findAll());
			//serviceAlquiler.add((new alquilerDTO(2,1,1,"23/7/2018")));
			
		//cliente	
			clienteDAO daocliente = new clienteDAOImpl();
			clienteService servicecliente = new clienteServiceImpl(daocliente);
			servicecliente.add(new clienteDTO(3,"clienteUno",1));
			
		//portada	
			portadaDAO daoportada = new portadaDAOImpl();
			portadaService serviceportada = new portadaServiceImpl(daoportada);
			
			serviceportada.add(new portadaDTO(1,"portadaUno",1));
		//update --> Falta hacer un finBy para traer objeto portada
			//serviceportada.update(dto);
			
			
		} catch (Exception e) {
			System.out.println(e.getMessage() + "\n");
			e.printStackTrace();
		}
	}

}
