package servicios;

import mvc.clienteDAO;
import mvc.clienteDTO;

public class clienteServiceImpl implements clienteService {
	
	private clienteDAO dao;
	
	public clienteServiceImpl(clienteDAO dao) {
		this.dao = dao;
	}
	
	public void add(clienteDTO clienteDto) throws Exception {
		dao.add(clienteDto);
	}

}
