package servicios;
import java.util.List;
import mvc.autorDTO;
import mvc.libroDTO;
import mvc.autorDAO;

public interface autorService {

    void addAutorLibro(autorDTO autorDTO, libroDTO libroDTO) throws Exception;
    autorDTO findById(int codigoDTO) throws Exception;
    void remove(int codigoDTO) throws Exception;
}