package servicios;

import java.util.List;

import mvc.alquilerDTO;
import mvc.libroDTO;

public interface alquilerService {
	
	void add(alquilerDTO dto) throws Exception;
	List<libroDTO> findAll() throws Exception;
	
}
