package servicios;

import java.util.List;

import mvc.libroDAO;
import mvc.libroDTO;

public class librosServiceImpl implements librosService{
	private libroDAO dao;
	
	public librosServiceImpl(libroDAO dao) {
		this.dao = dao;
	}

	@Override
	public void add(libroDTO dto) throws Exception {
		dao.add(dto);
	}

	
	@Override
	public libroDTO findById(int codigoDTO) throws Exception {
		return dao.read(codigoDTO);
	}
		
	@Override
	public List<libroDTO> findAll() throws Exception {
		return dao.readAll();
	}
	@Override
	public void remove(int codigoDTO) throws Exception {
		dao.delete(codigoDTO);
		
	}
	
	/*
	@Override
	public void edit(libroDTO dto) throws Exception {
		// TODO Auto-generated method stub
		
	}

	

	
	}
	*/
	}

