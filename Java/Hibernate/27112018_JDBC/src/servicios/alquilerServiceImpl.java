package servicios;

import java.util.List;

import mvc.alquilerDAO;
import mvc.alquilerDTO;
import mvc.autorDAO;
import mvc.autorDTO;
import mvc.libroDTO;

public class alquilerServiceImpl implements alquilerService {
	
	private alquilerDAO dao;
		
	public alquilerServiceImpl(alquilerDAO dao) {
		this.dao = dao;
	}
	
	public void add(alquilerDTO alquilerDto) throws Exception {
		dao.add(alquilerDto);
	}
	public List<libroDTO> findAll() throws Exception {
		return dao.readAll();
	}

	}

