package servicios;

import java.util.List;

import mvc.autorDTO;
import mvc.libroDTO;
import mvc.autorDAO;

public class autorServiceImpl implements autorService{
	
	private autorDAO dao;
	

	public autorServiceImpl(autorDAO dao) {
		this.dao = dao;
	}
	
	public void addAutorLibro(autorDTO autorDto, libroDTO libroDto) throws Exception {
		dao.addAutorLibro(autorDto, libroDto);
	}
	
	@Override
	public autorDTO findById(int codigoDTO) throws Exception {
		return dao.read(codigoDTO);
	}
	
	public void remove(int codigoDTO) throws Exception {
		dao.delete(codigoDTO);
	}	
}
