package servicios;
import java.util.List;

import mvc.libroDTO;

public interface librosService {

    void add(libroDTO dto) throws Exception;
	
	libroDTO findById(int codigoDTO) throws Exception;

	List<libroDTO> findAll() throws Exception;
	
	void remove(int codigoDTO) throws Exception;
	
	//void edit(libroDTO dto) throws Exception;
	
	
	
	//List<libroDTO> findAll() throws Exception;

	
}
