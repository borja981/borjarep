package mvc;
import mvc.libroDTO;
public interface autorDAO {

	void addAutorLibro(autorDTO autordto, libroDTO librodto) throws Exception;
	autorDTO read(int codigoDTO) throws Exception;
	void delete(int autorDTO) throws Exception;
}
