package mvc;
import java.util.List;

public interface libroDAO {
	
	void add(libroDTO dto) throws Exception;
	libroDTO read(int codigoDTO) throws Exception;
	//void update(libroDTO dto);
	void delete(int libroDTO) throws Exception;
	List<libroDTO> readAll() throws Exception;
}
