package mvc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import mvc.clienteDTO;
import JDBC.utilidades;

public class clienteDAOImpl implements clienteDAO {
	
	private static final String INSERT = "INSERT INTO bb_cliente(id_cliente, nombre_cliente, id_alquilerfk) VALUES (?,?,?)";
	
	private Connection getConnection() throws SQLException {
		return DriverManager.getConnection(utilidades.URL, utilidades.USER, utilidades.PASSWORD);
	}
	
	public void add(clienteDTO dto) throws Exception {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(INSERT);
			
			ps.setInt(1, dto.getId_cliente());
			ps.setString(2, dto.getNombre_cliente());
			ps.setInt(3, dto.getId_alquilerfk());

			
			System.out.println("cliente a�adido con exito!!, id = " + dto.getId_cliente());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new Exception("Fallo insercion en BBDD 1", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new Exception("Fallo cierre insercion en BBDD 2", e);
				}
			}
		}
	}

}
