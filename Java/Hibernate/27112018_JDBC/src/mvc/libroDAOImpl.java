package mvc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import JDBC.utilidades;
import mvc.libroDAO;
import mvc.libroDTO;
/*
 * Clase de implementación del DAO para JDBC sobre BBDD ORACLE
 * 
 * Utilizamos instrucciones parametrizadas (PreparedStatement)
 * 
 */
public class libroDAOImpl implements libroDAO {

	private static final String SELECT = "SELECT id_libro, titulo, id_autorfk FROM bb_libros WHERE id_libro = ?";
	private static final String INSERT = "INSERT INTO bb_libros (id_libro, titulo, id_autorfk) VALUES (?,?,?)";
	//private static final String UPDATE = "UPDATE bb_libros SET id_autorfk = ? WHERE id_libro = ?";
	private static final String DELETE = "DELETE FROM bb_libros WHERE id_libro = ?";
	private static final String SELECT_ALL = "SELECT id_libro, titulo, id_autorfk FROM bb_libros";

	private Connection getConnection() throws SQLException {
		return DriverManager.getConnection(utilidades.URL, utilidades.USER, utilidades.PASSWORD);
	}
	
	public void add(libroDTO dto) throws Exception {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(INSERT);

			ps.setInt(1, dto.getId_libro());
			ps.setString(2, dto.getTitulo());
			ps.setInt(3, dto.getId_autorfk());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new Exception("Fallo insercion en BBDD 1", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new Exception("Fallo cierre insercion en BBDD 2", e);
				}
			}
		}
	}

	public libroDTO read(int codigoDTO) throws Exception {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(SELECT);

			ps.setInt(1, codigoDTO);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				int id_libro = rs.getInt("id_libro");
				String titulo = rs.getString("titulo");
				int id_autorfk = rs.getInt("id_autorfk");
	

				return new libroDTO(id_libro, titulo, id_autorfk);

			} else {
				throw new Exception("No hay categoria con codigo " + codigoDTO);
			}

		} catch (SQLException e) {
			throw new Exception("Fallo lectura en BBDD dao 1", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new Exception("Fallo cierre lectura en BBDD dao 2", e);
				}
			}
		}
	}
	
	@Override
	public List<libroDTO> readAll() throws Exception {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(SELECT_ALL);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				List<libroDTO> libros = new ArrayList<>();

				int id_libro;
				String titulo;
				int id_autorfk;
		

				do {
					id_libro = rs.getInt("id_libro");
					titulo = rs.getString("titulo");
					id_autorfk = rs.getInt("id_autorfk");
					
					libros.add(new libroDTO(
							id_libro, titulo, id_autorfk));

				} while (rs.next());

				return libros;

			} else {
				throw new Exception("No hay categorias");
			}

		} catch (SQLException e) {
			throw new Exception("Fallo lectura completa en BBDD metodo readAll", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new Exception("Fallo cierre lectura completa en BBDD", e);
				}
			}
		}
	}
	
	public void delete(int codigoDTO) throws Exception {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(DELETE);

			ps.setInt(1, codigoDTO);

			ps.executeUpdate();
			System.out.println("Exito al borrar libro con id "+codigoDTO);

		} catch (SQLException e) {
			throw new Exception("Fallo eliminacion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new Exception("Fallo cierre eliminacion en BBDD", e);
				}
			}
		}
	}

}
	

