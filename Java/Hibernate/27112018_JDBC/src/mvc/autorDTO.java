package mvc;

public class autorDTO {
	
	private int id_autor;
	private String nombre;
	
	public int getId_autor() {
		return id_autor;
	}
	public void setId_autor(int id_autor) {
		this.id_autor = id_autor;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public autorDTO(int id_autor, String nombre) {
		super();
		this.id_autor = id_autor;
		this.nombre = nombre;
	}
	
	@Override
	public String toString() {
		return "autorDTO [id_autor=" + id_autor + ", nombre=" + nombre + "]";
	}
	

}
