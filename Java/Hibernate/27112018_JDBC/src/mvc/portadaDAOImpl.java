package mvc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import mvc.portadaDTO;
import JDBC.utilidades;

public class portadaDAOImpl implements portadaDAO {
	
	private static final String INSERT = "INSERT INTO bb_portada(id_portada, titulo_portada, id_librofk) VALUES (?,?,?)";
	private static final String UPDATE = "UPDATE bb_portada SET titulo_portada = ? WHERE id_portada = ?";
	
	private Connection getConnection() throws SQLException {
		return DriverManager.getConnection(utilidades.URL, utilidades.USER, utilidades.PASSWORD);
	}
	
	public void add(portadaDTO dto) throws Exception {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(INSERT);
			
			ps.setInt(1, dto.getId_portada());
			ps.setString(2, dto.getTitulo_portada());
			ps.setInt(3, dto.getId_librofk());

			
			System.out.println("portada a�adida a�adido con exito!!, id = " + dto.getId_portada());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new Exception("Fallo insercion en BBDD 1", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new Exception("Fallo cierre insercion en BBDD 2", e);
				}
			}
		}
	}

	@Override
	public void update(portadaDTO dto) throws Exception {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(UPDATE);

			ps.setInt(1, dto.getId_portada());
			ps.setString(2, dto.getTitulo_portada());
			ps.setInt(3, dto.getId_librofk());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new Exception("Fallo actualizacion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new Exception("Fallo cierre actualizacion en BBDD", e);
				}
			}
		}
	}

}
