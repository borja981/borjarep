package mvc;

public class portadaDTO {

	private int id_portada;
	private String titulo_portada;
	private int id_librofk;
	
	public portadaDTO(int id_portada, String titulo_portada, int id_librofk) {
		super();
		this.id_portada = id_portada;
		this.titulo_portada = titulo_portada;
		this.id_librofk = id_librofk;
	
	}

	public int getId_portada() {
		return id_portada;
	}

	public void setId_portada(int id_portada) {
		this.id_portada = id_portada;
	}

	public String getTitulo_portada() {
		return titulo_portada;
	}

	public void setTitulo_portada(String titulo_portada) {
		this.titulo_portada = titulo_portada;
	}

	public int getId_librofk() {
		return id_librofk;
	}

	public void setId_librofk(int id_librofk) {
		this.id_librofk = id_librofk;
	}

	@Override
	public String toString() {
		return "portadaDTO [id_portada=" + id_portada + ", titulo_portada=" + titulo_portada + ", id_librofk="
				+ id_librofk + "]";
	}
	
	
	
	
}
