package mvc;

public class libroDTO{
	
	private int id_libro;
	private String titulo;
	private int id_autorfk;
	
	public libroDTO(int id_libro, String titulo, int id_autorfk) {
		super();
		this.id_libro = id_libro;
		this.titulo = titulo;
		this.id_autorfk = id_autorfk;
	}
	
	public libroDTO(String titulo) {
		this.titulo= titulo;
	}

	public int getId_libro() {
		return id_libro;
	}

	public void setId_libro(int id_libro) {
		this.id_libro = id_libro;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getId_autorfk() {
		return id_autorfk;
	}

	public void setId_autorfk(int id_autorfk) {
		this.id_autorfk = id_autorfk;
	}

	@Override
	public String toString() {
		return "libroDTO [id_libro=" + id_libro + ", titulo=" + titulo + ", id_autorfk=" + id_autorfk + "]";
	}

}
