package mvc;

public class clienteDTO {

	private int id_cliente;
	private String nombre_cliente;
	private int id_alquilerfk;
	
	public clienteDTO(int id_cliente, String nombre_cliente, int id_alquilerfk) {
		super();
		this.id_cliente = id_cliente;
		this.nombre_cliente = nombre_cliente;
		this.id_alquilerfk = id_alquilerfk;
	}

	public int getId_cliente() {
		return id_cliente;
	}

	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}

	public String getNombre_cliente() {
		return nombre_cliente;
	}

	public void setNombre_cliente(String nombre_cliente) {
		this.nombre_cliente = nombre_cliente;
	}

	public int getId_alquilerfk() {
		return id_alquilerfk;
	}

	public void setId_alquilerfk(int id_alquilerfk) {
		this.id_alquilerfk = id_alquilerfk;
	}

	@Override
	public String toString() {
		return "clienteDTO [id_cliente=" + id_cliente + ", nombre_cliente=" + nombre_cliente + ", id_alquilerfk="
				+ id_alquilerfk + "]";
	}
	
	
}
