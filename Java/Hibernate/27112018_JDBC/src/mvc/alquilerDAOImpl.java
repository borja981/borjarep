package mvc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import JDBC.utilidades;

public class alquilerDAOImpl implements alquilerDAO {
	
	private static final String INSERT = "INSERT INTO bb_alquiler(id_alquiler, id_clientefk, id_librofk, fecha_alquiler) VALUES (?,?,?,?)";
	private static final String SELECT_ALL = "SELECT id_libro, titulo, id_autorfk from bb_libros where id_libro in (select id_librofk from bb_alquiler)";
	
	private Connection getConnection() throws SQLException {
		return DriverManager.getConnection(utilidades.URL, utilidades.USER, utilidades.PASSWORD);
	}
	
	public void add(alquilerDTO dto) throws Exception {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(INSERT);
			
			ps.setInt(1, dto.getId_alquiler());
			ps.setInt(2, dto.getId_clientefk());
			ps.setInt(3, dto.getId_librofk());
			ps.setString(4, dto.getFecha_alquiler());
			

			ps.executeUpdate();
			System.out.println("alquiler realizado con �xito, id = " + dto.getId_alquiler());
		} catch (SQLException e) {
			throw new Exception("Fallo insercion en BBDD 1", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new Exception("Fallo cierre insercion en BBDD 2", e);
				}
			}
		}
	}
	
	public List<libroDTO> readAll() throws Exception {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(SELECT_ALL);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				List<libroDTO> libros = new ArrayList<>();
				
				int idlibro;
				String titulo;
				int idautorfk;

				do {
					idlibro = rs.getInt("id_libro");
					titulo = rs.getString("titulo");
					idautorfk = rs.getInt("id_autorfk");
					
					libros.add(new libroDTO(idlibro,titulo,idautorfk));

				} while (rs.next());

				return libros;

			} else {
				throw new Exception("No hay libros alquilados que mostrar");
			}

		} catch (SQLException e) {
			throw new Exception("Fallo lectura completa en BBDD metodo readAll de alquileres", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new Exception("Fallo cierre lectura completa en BBDD", e);
				}
			}
		}
	}
	
}
