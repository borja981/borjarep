package mvc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import mvc.autorDAO;
import JDBC.utilidades;

public class autorDAOImpl implements autorDAO {
	
	private static final String SELECT = "SELECT id_autor, nombre from bb_autor WHERE id_autor = ?";
	private static final String INSERT = "INSERT INTO bb_autor (id_autor, nombre) VALUES (?,?)";
	private static final String INSERT_2 = "INSERT INTO bb_libros (id_libro, titulo, id_autorfk) VALUES (?,?,?)";
	private static final String DELETE = "DELETE FROM bb_autor WHERE id_autor = ?";
	
	private Connection getConnection() throws SQLException {
		return DriverManager.getConnection(utilidades.URL, utilidades.USER, utilidades.PASSWORD);
	}
	
	
	public void addAutorLibro(autorDTO dtoAutor, libroDTO dtoLibro) throws Exception {
		Connection cn = null;
		try {
			cn = getConnection();
			
			//Para insertar autor
			PreparedStatement psAutor = cn.prepareStatement(INSERT);

			psAutor.setInt(1, dtoAutor.getId_autor());
			psAutor.setString(2, dtoAutor.getNombre());
			
			
			psAutor.executeUpdate();
			cn.getAutoCommit(); //Commit en primera transacción
		
			//Para insertar libro
			PreparedStatement psLibro = cn.prepareStatement(INSERT_2);

			psLibro.setInt(1, dtoLibro.getId_libro());
			psLibro.setString(2, dtoLibro.getTitulo());
			psLibro.setInt(3, dtoLibro.getId_autorfk());
			
			psLibro.executeUpdate();
			
				
		} catch (SQLException e) {
			cn.rollback(); 
			throw new Exception("Fallo insercion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new Exception("Fallo cierre insercion en BBDD", e);
				}
			}
		}
	}
	
	public autorDTO read(int codigoDTO) throws Exception {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(SELECT);

			ps.setInt(1, codigoDTO);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				int id_autor = rs.getInt("id_autor");
				String nombre = rs.getString("nombre");
	

				return new autorDTO(id_autor, nombre);

			} else {
				throw new Exception("No hay categoria con codigo " + codigoDTO);
			}

		} catch (SQLException e) {
			throw new Exception("Fallo lectura en BBDD dao 1", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new Exception("Fallo cierre lectura en BBDD dao 2", e);
				}
			}
		}
	}
	
	public void delete(int codigoDTO) throws Exception {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(DELETE);

			ps.setInt(1, codigoDTO);

			ps.executeUpdate();
			System.out.println("Exito al borrar autor con id "+codigoDTO);

		} catch (SQLException e) {
			throw new Exception("Fallo eliminacion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new Exception("Fallo cierre eliminacion en BBDD", e);
				}
			}
		}
	}
}
