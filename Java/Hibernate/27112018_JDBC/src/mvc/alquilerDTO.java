package mvc;

public class alquilerDTO {
	
	private int id_alquiler;
	private int id_librofk;
	private int id_clientefk;
	private String fecha_alquiler;
	
	public alquilerDTO(int id_alquiler, int id_librofk, int id_clientefk, String fecha_alquiler) {
		super();
		this.id_alquiler = id_alquiler;
		this.id_librofk = id_librofk;
		this.id_clientefk = id_clientefk;
		this.fecha_alquiler = fecha_alquiler;
	}

	public int getId_alquiler() {
		return id_alquiler;
	}

	public void setId_alquiler(int id_alquiler) {
		this.id_alquiler = id_alquiler;
	}

	public int getId_librofk() {
		return id_librofk;
	}

	public void setId_librofk(int id_librofk) {
		this.id_librofk = id_librofk;
	}

	public int getId_clientefk() {
		return id_clientefk;
	}

	public void setId_clientefk(int id_clientefk) {
		this.id_clientefk = id_clientefk;
	}

	public String getFecha_alquiler() {
		return fecha_alquiler;
	}

	public void setFecha_alquiler(String fecha_alquiler) {
		this.fecha_alquiler = fecha_alquiler;
	}

	@Override
	public String toString() {
		return "alquilerDTO [id_alquiler=" + id_alquiler + ", id_librofk=" + id_librofk + ", id_clientefk="
				+ id_clientefk + ", fecha_alquiler=" + fecha_alquiler + "]";
	}
	
	
	
}
