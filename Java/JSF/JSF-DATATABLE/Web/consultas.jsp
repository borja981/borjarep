<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<f:view>
	<html>
<head>
<meta charset="ISO-8859-1">
<title>Ejemplo DataTable Basico JSF</title>
</head>
<body>
	<h:outputText value="#{data.alumno.nombre}" />
	<br/>
	<h:outputText value="#{data.alumno.apellidos}" />
	<br/>		
	<h:outputText value="#{data.alumno.email}" />
	<br/>
	<h:outputText value="#{data.alumno.telefono}" />
	<br/> 
		
</body>
	</html>
</f:view>