<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Insert title here</title>
</head>
<body>
	<f:view>
		<h:dataTable var="alumno" value="#{data.students}">

			<h:column>

				<f:facet name="header">NOMBRE</f:facet>

			#{alumno.nombre}

			</h:column>

			<h:column>
				<f:facet name="header">APELLIDOS</f:facet>
				<br />
				<h:outputText value="#{alumno.apellidos}" />
			</h:column>

			<h:column>
				<f:facet name="header">EMAIL</f:facet>
				<br />
				<h:outputText value="#{alumno.email}" />
			</h:column>

			<h:column>
				<f:facet name="header">TELEFONO</f:facet>
				<br />
				<h:outputText value="#{alumno.telefono}" />
			</h:column>
			<h:column>

			</h:column>
		</h:dataTable>
	</f:view>
</body>
</html>