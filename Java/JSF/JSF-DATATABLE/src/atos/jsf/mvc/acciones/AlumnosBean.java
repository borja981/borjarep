package atos.jsf.mvc.acciones;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import atos.jsf.mvc.modelo.Alumno;

@ManagedBean(name = "data")
@ApplicationScoped
public class AlumnosBean {
	
	private Alumno alumno;
	
	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	private List<Alumno> students;

	public List<Alumno> getStudents() {
		students = new ArrayList<>();

		students.add(new Alumno("Daniel", "Lopez Sanz", "daniel@telefonica.net", "678112233"));

		students.add(new Alumno("Elena", "Sanz Garcia", "elena@telefonica.net", "678445566"));

		students.add(new Alumno("Sergio", "Garcia Arranz", "sergio@telefonica.net", "678778899"));

		return students;
	}

	public void setStudents(List<Alumno> students) {
		this.students = students;
	}

	public String buscarAlumno(String nombre) {
		System.out.println("estoy aqui");

		for(Alumno alu : students){
			if(alu.getNombre()==(nombre)){
				System.out.println(alu.getNombre());
				alumno = alu;
			}
		}
		return "consultar";
	}
}
