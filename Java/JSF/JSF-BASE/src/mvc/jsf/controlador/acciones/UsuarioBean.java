package mvc.jsf.controlador.acciones;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/*
 * Clase POJO que encapsula un Managed Bean (bean gestionado por JSF)
 * que encapsula un usuario dentro de la apliaci�n.
 * 
 * Un managed bean ES SIEMPRE UN ATRIBUTO
 * 
 * El atributo (scope) puede ser:
 * request, session, application, view y ninguno
 * 
 * Hay que configurar mediante XML o con anotaciones
 * 
 * @ManagedBean => Si no se utiliza atributo name se crea
 * atributo con nombre de la clase en CaMeL. usuarioBean en este
 * caso
 */
@ManagedBean(name="usuario")
@SessionScoped
public class UsuarioBean implements Serializable {

	private static final long serialVersionUID = 666L;

	private String nombre;
	private String email;
	private String telefono = "913060606";
	
	public UsuarioBean() {
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	/*
	 * M�todos de negocio.
	 * 
	 * Devuelven una cadena indicando la vista a la que ir
	 * una vez ejecutado el codigo Java (logica de aplicacion)
	 */
	public String validar() {
		// Logica de negocio
		
		// Recuperar primer caracter de la propiedad telefon
		char first = telefono.charAt(0);
		
		if(first == '9') {
			// Ir a la pagina fijo.jsp
			return "fijo";
		}
		
		// Ir a la pagina movil.jsp
		return "movil";
		
	}
	
	public String borrar() {
		
		this.nombre="";
		return "welcomeJSF";
		}
		
	}
	
	

