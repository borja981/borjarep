<%-- 
	Reenviar peticion a welcomeJSF.jsp 

	Hay que hacer que la peticion pase por el Servlet de JSF
	
	Lo que hace es utilizar "ViewResolver" para cambiar la
	extension .faces por .jsp
--%>
<jsp:forward page="welcomeJSF.faces" />
