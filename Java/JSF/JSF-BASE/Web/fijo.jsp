<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%-- Libreria de etiquetas basica de JSF --%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core"%>

<%-- Libreria de etiquetas HTML de JSF --%>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<!DOCTYPE html>

<%-- 
	 Vista JSF. Vualquier etiqueta JSF (de cualquiera de las
	 2 librerias) van situadas SIEMPRE dentro de la etiqueta
	 f:view
--%>
<f:view>
	<html>
<head>
<meta charset="ISO-8859-1">
<title>Aplicacion Basica JSF</title>
</head>
<body bgcolor="gray" text="black">
	<h3>Registro usuario telefono fijo</h3>
</body>
	</html>
</f:view>