package filtro;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

/**
 * Filtro configurado para interceptar peticiones realizadas al Servlet
 */
@WebFilter("/DispatcherServlet")
public class FiltroValidacion implements Filter {

	public void destroy() {
		System.out.println("Filtro finalizado");
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
	
		HttpServletRequest requestHttp = (HttpServletRequest) request;

		String vista = "/error.jsp";
		
		String letra = request.getParameter("letra");
		String opcion = request.getParameter("opc");
		
		if(opcion =="a") {
			System.out.println("en metodos de "+ opcion);
			vista="juego.jsp";
			RequestDispatcher dispatcher = 
    				request.getRequestDispatcher(vista);
		}
		

		
	       int numero;
	        if (isNumeric(letra) == true) {
	            numero = Integer.parseInt(letra);
	            System.out.println("Numero: " + numero);
	            RequestDispatcher dispatcher = 
	    				request.getRequestDispatcher(vista);

	    		dispatcher.forward(request, response);
	        } else {
	            System.out.println("No es un numero");
	            chain.doFilter(request,response);
	        }

	    }
		
	
	public void init(FilterConfig fConfig) throws ServletException {
		System.out.println("Filtro inicializado");
	}
	
	private static boolean isNumeric(String cadena){
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe){
			return false;
		}
	}

}

