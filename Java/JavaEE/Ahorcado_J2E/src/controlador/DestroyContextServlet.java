package controlador;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DestroyContextServlet
 */
@WebServlet("/DestroyContextServlet")
public class DestroyContextServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DestroyContextServlet() {
        super();
    }    
        // TODO Auto-generated constructor stub
        protected void service(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {
        	
         	HttpSession session = request.getSession();
        	ServletContext context = session.getServletContext();
        	
           	String opcion = request.getParameter("opc");

    			System.out.println("metodo reset");
    			
    			String vidas = (String) context.getAttribute("vidas");
    			vidas = "5";
    			
    			context.setAttribute("vidas", vidas);
    			String url = "juego.jsp";
    			RequestDispatcher dispatcher = request.getRequestDispatcher(url);
    	    	
    			dispatcher.forward(request, response);
    			
        }
}