package controlador;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import action.Acciones;

/**
 * Servlet implementation class crearPartida
 */
@WebServlet("/DispatcherServlet")
public class DispatcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DispatcherServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
    	
    	HttpSession session = request.getSession();
    	ServletContext context = session.getServletContext();
    	
    		
		String url = "/resultado.jsp";
		String vidas = (String) context.getAttribute("vidas");
		
		int result = Integer.parseInt(vidas);
		if(result==0) {
			
		 url = "/fin.jsp";
		}
    	
    	Acciones partida = new Acciones();
    	String mensaje = partida.execute(request);
    	
    	System.out.println("datos partida: " +partida);
    	
    	System.out.println("aqui en servlet");

    	
    	RequestDispatcher dispatcher = request.getRequestDispatcher(url);
    	
		dispatcher.forward(request, response);
		}
    }


