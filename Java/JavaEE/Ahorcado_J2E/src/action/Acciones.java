package action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class Acciones {

	public String execute(HttpServletRequest request) {

		System.out.println("aqui en acciones");

		// Recuperar sesion (usuario) que se acaba de crear
		HttpSession session = request.getSession();

		// Recuperar app donde se acaba de crear una sesion
		ServletContext context = session.getServletContext();

		String mensaje="";
		String cadenaDondeBuscar = (String) context.getAttribute("palabra");
		String loQueQuieroBuscar = request.getParameter("letra");

		String[] palabras = loQueQuieroBuscar.split("\\s+");

		for (String palabra : palabras) {
			if (cadenaDondeBuscar.contains(palabra)) {
				System.out.println("Encontrado");
				mensaje= "letra encontrada";
				/*
					List<String>aciertos = (List<String>) context.getAttribute("aciertos");
					aciertos.add(palabra);
					context.setAttribute("aciertos", aciertos);
				 */
				session.setAttribute("mensaje", mensaje);
			}else {  
				System.out.println("No Encontrado");		
				session.setAttribute("mensaje", "letra no encontrada");
				mensaje= "letra no encontrada";

				String vidas = (String) context.getAttribute("vidas");
				int numeroVidas = Integer.parseInt(vidas);
				numeroVidas = numeroVidas-1;
				vidas = String.valueOf(numeroVidas);
				context.setAttribute("vidas", vidas);

			}

		}
		return mensaje;
	}
}


