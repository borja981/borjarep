package listener;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/*
 * Listener (gestor de eventos) para aplicacion (ServletContext)
 */
@WebListener
public class ContextListener implements ServletContextListener {

	

	public void contextInitialized(ServletContextEvent sce) {
		// Recuperar app que se acaba de crear
		ServletContext context = sce.getServletContext();
		
		List<String> lista = new ArrayList<>();	
		
		context.setAttribute("lista",lista);
		
	}

	public void contextDestroyed(ServletContextEvent sce) {
		System.out.println("Replegada del servidor la app");
	}
	
}
