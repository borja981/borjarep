package controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/ControladorChat")
public class DispatcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
    	
    	if(request.getParameter("borrar").equals("1")){
    		request.getSession().getServletContext().setAttribute("lista", new ArrayList<>());
    		
    	}else {

    	String nombre = request.getParameter("nombre");
    	String mensaje = request.getParameter("mensaje");
    	
    	System.out.println("nombre jsp: "+nombre);
    	
    	HttpSession session = request.getSession();
    	ServletContext context = session.getServletContext();
    	
    	context.setAttribute("nombre", nombre);
		context.setAttribute("mensaje", mensaje);
		
		List <String> lista = (List<String>) context.getAttribute("lista");
		
		System.out.println(nombre);
		lista.add("Usuario: "+nombre+ "\nMensaje: "+mensaje);

		context.setAttribute("lista", lista);
		
    	RequestDispatcher dispatcher = request.getRequestDispatcher("resultado.jsp");
		dispatcher.forward(request, response);
    	
    	}   	
    }
    	
}




