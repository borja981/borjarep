package entidades;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Libro
 *
 */
@Entity
@Table(name="BB_LIBROS")
public class Libro implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	// DI para indicar como se genera esa clave en la tabla
	//@GeneratedValue(strategy=GenerationType.AUTO)
	
	@Id
	@Column(name="ID_LIBRO")
	private int id_libro;

	@Column(name="TITULO")
	private String titulo;
	
	@Column(name="ID_AUTORFK")
	private int id_autorfk;

	public int getId_libro() {
		return id_libro;
	}

	public void setId_libro(int id_libro) {
		this.id_libro = id_libro;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getId_autorfk() {
		return id_autorfk;
	}

	public void setId_autorfk(int id_autorfk) {
		this.id_autorfk = id_autorfk;
	}

	public Libro(int id_libro, String titulo, int id_autorfk) {
		super();
		this.id_libro = id_libro;
		this.titulo = titulo;
		this.id_autorfk = id_autorfk;
	}
	
	public Libro() {
		super();
		
		
	}

	@Override
	public String toString() {
		return "Libro [id_libro=" + id_libro + ", titulo=" + titulo + ", id_autorfk=" + id_autorfk + "]";
	}
	
	
}
