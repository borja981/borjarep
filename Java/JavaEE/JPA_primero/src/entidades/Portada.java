package entidades;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Portada
 *
 */
@Entity
@Table(name="BB_PORTADA")
public class Portada implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	// DI para indicar como se genera esa clave en la tabla
	//@GeneratedValue(strategy=GenerationType.AUTO)
	
	@Id
	@Column(name="ID_PORTADA")
	private int id_portada;

	@Column(name="TITULO_PORTADA")
	private String titulo_portada;
	
	@OneToOne(cascade = {}, fetch = FetchType.EAGER)
	@JoinColumn(name="ID_LIBROFK", unique = false, nullable = true, insertable =  true, updatable = true)
	private Libro libro;
	public Libro getLibro(){
		return libro;
	}
	
	
	
	public int getId_portada() {
		return id_portada;
	}
	
	
	
	public void setId_portada(int id_portada) {
		this.id_portada = id_portada;
	}

	public String getTitulo_portada() {
		return titulo_portada;
	}

	public void setTitulo_portada(String titulo_portada) {
		this.titulo_portada = titulo_portada;
	}
	
	
	public Portada(int id_portada, String titulo_portada, Libro libro) {
		super();
		this.id_portada = id_portada;
		this.titulo_portada = titulo_portada;
		this.libro = libro;
	}

	public Portada() {
		super();
		
		
	}
   
}
