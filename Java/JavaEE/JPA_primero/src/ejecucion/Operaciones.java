package ejecucion;


import controlador.LibrosDAO;
import controlador.LibrosDAOImpl;
import controlador.PortadasDAO;
import controlador.PortadasDAOImpl;
import entidades.Libro;
import entidades.Portada;

public class Operaciones {

	public static void main(String[] args) {

		PortadasDAO daoPortadas = new PortadasDAOImpl();
		LibrosDAO daoLibros = new LibrosDAOImpl();

		System.out.println("Unidad de Persitencia cargada");

		//Para a�adir portada buscamos un libro
		Libro libro = daoLibros.findById(1);
		Portada portada = new Portada(8, "Dune_portada", libro);

		// Crear portada
		daoPortadas.create(portada);

		// Recuperar entidad por codigo
		portada = daoPortadas.findById(2);
		System.out.println(portada.getTitulo_portada());
		System.out.println(portada.getLibro().getTitulo());

		//Borrar portada
		daoPortadas.delete(1);

		//Crear libro
		daoLibros.create(libro);

		// Recuperar entidad por codigo
		portada = daoPortadas.findById(1);
		System.out.println(portada.getTitulo_portada());

		//Borrar portada
		daoLibros.delete(1);
	}

}
