package controlador;

import entidades.Libro;

public interface LibrosDAO {
	void create(Libro libro);
	Libro findById(int libro);
	void delete(int libro);
}
