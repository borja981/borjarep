package controlador;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entidades.Portada;

public class PortadasDAOImpl implements PortadasDAO{

	private EntityManagerFactory emf;

	public PortadasDAOImpl() {

		emf = Persistence.createEntityManagerFactory("JPA_primero");
	}

	private EntityManager getEntityManager() {
		return emf.createEntityManager();
	}
	////////////////////////////////////////////////////////////////////////
	public void create(Portada portada) /* throws Exception */ {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			em.persist(portada);
			em.getTransaction().commit();

		}finally {
			if (em != null) {
				em.close();
			}
		}
	}
	////////////////////////////////////////////////////////////////////////

	public Portada findById(int idPortada) {
		EntityManager em = null;
		try {
			em = getEntityManager();


			return em.find(Portada.class, idPortada);

		} finally {
			if (em != null) {
				em.close();
			}
		}
	}
	////////////////////////////////////////////////////////////////////////
	// Eliminar portada creada
	public void delete(int idPortada) {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Abrir transaccion
			em.getTransaction().begin();

			// Recuperar instancia de entidad desde EntityManager
			// para que sea ATTACH
			Portada portada = em.getReference(Portada.class, idPortada);

			// Eliminar instancia entidad (hacer DELETE en BBDD
			// asociada al EntityManagerFactory con el que hemos
			// creado nuestro EntityManager)
			em.remove(portada);

			// Confirmar transaccion
			em.getTransaction().commit();
			System.out.println("portada eliminada");

		} finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}

}
