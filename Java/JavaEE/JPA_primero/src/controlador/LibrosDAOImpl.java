package controlador;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entidades.Libro;
import entidades.Portada;

public class LibrosDAOImpl implements LibrosDAO{
	
	private EntityManagerFactory emf;
	
	public LibrosDAOImpl() {
		
		emf = Persistence.createEntityManagerFactory("JPA_primero");
	}
	
	private EntityManager getEntityManager() {
		return emf.createEntityManager();
	}
	////////////////////////////////////////////////////////////////////////
	public void create(Libro libro) /* throws Exception */ {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			em.persist(libro);
			em.getTransaction().commit();

		}finally {
			if (em != null) {
				em.close();
			}
		}
	}
	////////////////////////////////////////////////////////////////////////

	public Libro findById(int idLibro) {
		EntityManager em = null;
		try {
			em = getEntityManager();


			return em.find(Libro.class, idLibro);

		} finally {
			if (em != null) {
				em.close();
			}
		}
	}
	////////////////////////////////////////////////////////////////////////
	// Eliminar portada creada
	public void delete(int idLibro) {
		EntityManager em = null;
		try {

			em = getEntityManager();

			em.getTransaction().begin();

			Portada portada = em.getReference(Portada.class, idLibro);
			em.remove(portada);
			
			// Confirmar transaccion
			em.getTransaction().commit();
			System.out.println("portada eliminada");

		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

}

