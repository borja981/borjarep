package controlador;

import entidades.Portada;

public interface PortadasDAO {
	
	void create(Portada portada);
	Portada findById(int idportada);
	void delete(int idPortada);
}