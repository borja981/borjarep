package controlador;

import entidades.Rol;

public interface RolDAO {
	
	void addRol(Rol rol);
	
}
