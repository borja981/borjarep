package controlador;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entidades.Rol;
import entidades.Usuario;

public class RolDAOImpl implements RolDAO{
	
	private EntityManagerFactory emf;

	public RolDAOImpl() {

		emf = Persistence.createEntityManagerFactory("PU");
	}

	private EntityManager getEntityManager() {
		return emf.createEntityManager();
	}
	
	public void addRol(Rol rol) {
		EntityManager em = null;
		try {
			
			em = getEntityManager();
			em.getTransaction().begin();
			em.persist(rol);
			em.getTransaction().commit();

			
			System.out.println("--> Rol creado con exito!!");

		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

}
	
