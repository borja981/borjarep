package controlador;

import entidades.Usuario;
import javax.persistence.*;

public class UsuarioDAOImpl implements UsuarioDAO{
	
	private EntityManagerFactory emf;

	public UsuarioDAOImpl() {

		emf = Persistence.createEntityManagerFactory("PU");
	}

	private EntityManager getEntityManager() {
		return emf.createEntityManager();
	}
	
	public Usuario findById(int idUser) {
		EntityManager em = null;
		try {
			em = getEntityManager();
			System.out.println("en dao usuario");

			return em.find(Usuario.class, idUser);

		} finally {
			if (em != null) {
				em.close();
			}
		}
	}
}
