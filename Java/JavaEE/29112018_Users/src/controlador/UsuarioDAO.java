package controlador;

import entidades.Usuario;

public interface UsuarioDAO {

	Usuario findById(int idusuario);

}
