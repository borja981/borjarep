drop table AA_USUARIO;
CREATE TABLE AA_USUARIO(
id_usuario int primary key not null,
nombre varchar(20),
id_rolfk int,
id_passwordfk int);

CREATE TABLE AA_PASSWORD(
id_password int primary key not null,
pass varchar(20));

drop table aa_rol;
CREATE TABLE AA_ROL(
id_rol int primary key not null,
id_inventariofk int);

CREATE TABLE AA_PERMISOS(
id_permiso int primary key not null,
id_rolfk int,
id_inventariofk int);

drop table aa_inventario;
CREATE TABLE AA_INVENTARIO(
id_inventario int primary key not null,
id_rolfk int);

ALTER TABLE AA_USUARIO
ADD CONSTRAINT fk_passwordFK
  FOREIGN KEY (id_passwordfk)
  REFERENCES AA_PASSWORD(id_password);
  
ALTER TABLE AA_USUARIO
ADD CONSTRAINT fk_rolFK
  FOREIGN KEY (id_rolfk)
  REFERENCES AA_ROL(id_rol);
  
ALTER TABLE AA_PERMISOS
ADD CONSTRAINT fk_rolFKpermiso
  FOREIGN KEY (id_rolfk)
  REFERENCES AA_ROL(id_rol);
  
ALTER TABLE AA_PERMISOS
ADD CONSTRAINT fk_inventarioFKpermiso
  FOREIGN KEY (id_inventariofk)
  REFERENCES AA_INVENTARIO(id_inventario);
  
alter table aa_rol drop constraint PK_LIBROS_CODIGO;
ALTER TABLE aa_inventario drop column id_rolfk;
ALTER TABLE aa_inventario add nombre_rol varchar(20);

  
  insert into AA_USUARIO (id_usuario, nombre, id_rolfk, id_passwordfk)values(2,'usuario',null,1);
  insert into aa_password (id_password, pass)values(2,'abdwne');
  insert into aa_rol (id_rol, nombre_rol)values(2,'admin');
  insert into aa_inventario (id_inventario, nombre_inventario, nombre_rol)values(1,'Crear documentos', null);
  insert into aa_inventario (id_inventario, nombre_inventario, nombre_rol)values(2,'Editar documentos', null);
  insert into aa_inventario (id_inventario, nombre_inventario, nombre_rol)values(3,'leer documentos', null);
  insert into aa_permisos (id_permiso, id_rolfk, id_inventariofk)values(1,1,3);
  insert into aa_permisos (id_permiso, id_rolfk, id_inventariofk)values(4,2,3);
  
  
  delete from aa_inventario where id_inventario = 2;
  
  select *  from aa_usuario;
  select * from aa_password;
  select * from aa_inventario;
  select * from aa_rol;
  select * from aa_permisos;
  
  
  
