package entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;
import entidades.Permisos;


@Entity
@Table(name="AA_INVENTARIO")
public class Inventario implements Serializable{
	
    private static final long serialVersionUID = 1L;
	@Id
	@Column(name="ID_INVENTARIO")
	private int idInventario;
	
	@OneToMany(mappedBy="inventario") //hace referencia al objeto inventario de la tabla permisos
	private List<Permisos> permisos;
	
	@Column(name="nombre_inventario")
	private String nombre_inventario;

	public Inventario() {
		super();
		
	}

	public Inventario(int idInventario, List<Permisos> permisos, String nombre_inventario) {
		super();
		this.idInventario = idInventario;
		this.permisos = permisos;
		this.nombre_inventario = nombre_inventario;
	}

	public int getIdInventario() {
		return idInventario;
	}

	public void setIdInventario(int idInventario) {
		this.idInventario = idInventario;
	}


	public List<Permisos> getPermisos() {
		return permisos;
	}

	public void setPermisos(List<Permisos> permisos) {
		this.permisos = permisos;
	}

	public String getNombre_inventario() {
		return nombre_inventario;
	}

	public void setNombre_inventario(String nombre_inventario) {
		this.nombre_inventario = nombre_inventario;
	}   
	
	
}
