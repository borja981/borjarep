package entidades;
import java.io.Serializable;
import java.util.List;

import javax.persistence.*;



@Entity
@Table(name="AA_PASSWORD")
public class Password implements Serializable{


    private static final long serialVersionUID = 1L;
    @Id
	@Column(name="ID_PASSWORD")
	private long idPassword;

	private String pass;

	public Password() {

	}

	public Password(long idPassword, String pass) {
		super();
		this.idPassword = idPassword;
		this.pass = pass;
	}

	@Override
	public String toString() {
		return "Password [idPassword=" + idPassword + ", pass=" + pass + "]";
	}

	
	
	
}
