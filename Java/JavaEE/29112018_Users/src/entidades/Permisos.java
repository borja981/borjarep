package entidades;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="AA_PERMISOS")
public class Permisos implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    private int id_permiso;
	@ManyToOne
	@JoinColumn(name = "id_rolfk")
    private Rol rol;
	@ManyToOne
	@JoinColumn(name = "id_inventariofk")
    private Inventario inventario;
	
	public Permisos() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Permisos(int id_permiso, Rol rol, Inventario inventario) {
		super();
		this.id_permiso = id_permiso;
		this.rol = rol;
		this.inventario = inventario;
	}

	public int getId_permiso() {
		return id_permiso;
	}

	public void setId_permiso(int id_permiso) {
		this.id_permiso = id_permiso;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public Inventario getInventario() {
		return inventario;
	}

	public void setInventario(Inventario inventario) {
		this.inventario = inventario;
	}

	@Override
	public String toString() {
		return "Permisos [id_permiso=" + id_permiso + ", rol=" + rol + ", inventario=" + inventario + "]";
	}
	
	
}
