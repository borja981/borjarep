package entidades;

import java.io.Serializable;
import java.util.List;
import entidades.Inventario;

import javax.persistence.*;


@Entity
@Table(name="AA_ROL")
public class Rol implements Serializable{


    private static final long serialVersionUID = 1L;
    @Id
    private int id_rol;
    private String nombre_rol;
	@OneToOne(mappedBy = "rol")
    private Usuario usuario;
    @OneToMany(mappedBy="rol")
	private List<Permisos> permiso;

	public Rol() {
		super();
	}

	public Rol(int id_rol, String nombre_rol, Usuario usuario, List<Permisos> permiso) {
		super();
		this.id_rol = id_rol;
		this.nombre_rol = nombre_rol;
		this.usuario = usuario;
		this.permiso = permiso;
	}
	
	

	public Rol(int id_rol, String nombre_rol) {
		super();
		this.id_rol = id_rol;
		this.nombre_rol = nombre_rol;
	}

	public Rol(int id_rol, String nombre_rol, Usuario usuario) {
		super();
		this.id_rol = id_rol;
		this.nombre_rol = nombre_rol;
		this.usuario = usuario;
	}

	public int getId_rol() {
		return id_rol;
	}

	public void setId_rol(int id_rol) {
		this.id_rol = id_rol;
	}

	public String getNombre_rol() {
		return nombre_rol;
	}

	public void setNombre_rol(String nombre_rol) {
		this.nombre_rol = nombre_rol;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Permisos> getPermiso() {
		return permiso;
	}

	public void setPermiso(List<Permisos> permiso) {
		this.permiso = permiso;
	}

	@Override
	public String toString() {
		return "Rol [id_rol=" + id_rol + ", nombre_rol=" + nombre_rol + ", usuario=" + usuario + ", permiso=" + permiso
				+ "]";
	}

	
}
