package entidades;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="AA_USUARIO")
public class Usuario implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="ID_USUARIO")
	private int id_usuario;
	@Column(name="NOMBRE")
	private String nombre;
	@JoinColumn(name="id_rolfk")
	private Rol rol;
	@JoinColumn(name="ID_PASSWORDFK")
	private Password pass;
	
	public Usuario() {
		
	}
	
	public Usuario(int id_usuario, String nombre, Rol rol, Password pass) {
		super();
		this.id_usuario = id_usuario;
		this.nombre = nombre;
		this.rol = rol;
		this.pass = pass;
	}

	public int getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public Password getPass() {
		return pass;
	}

	public void setPass(Password pass) {
		this.pass = pass;
	}
	
	
	
	
}

