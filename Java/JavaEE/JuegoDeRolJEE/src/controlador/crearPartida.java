package controlador;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import action.CrearPartidaAction;

/**
 * Servlet implementation class crearPartida
 */
@WebServlet("/crearPartida")
public class crearPartida extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public crearPartida() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

    	CrearPartidaAction partida = new CrearPartidaAction();
    	partida.execute(request);
    	
    	String url = "/resultado.jsp";
    	
    	RequestDispatcher dispatcher = request.getRequestDispatcher(url);

		dispatcher.forward(request, response);
    	
    }

}
