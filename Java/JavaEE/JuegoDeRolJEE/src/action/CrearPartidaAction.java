package action;

import javax.servlet.http.HttpServletRequest;

public class CrearPartidaAction {
	
	public String execute(HttpServletRequest request) {
		
		String mensaje;
		String nombre = request.getParameter("nombre");
    	String mazmorra = request.getParameter("mazmorra");
    	
		int numEntero = Integer.parseInt(mazmorra);	
		int numerosDados = (int) (Math.random() * 2) + 1;
		
		if(numerosDados == numEntero ) {
			 mensaje="\r\n" + 
			 		"YOU HAVE DEFEATED MY DOG";
		}else {
			 mensaje="YOU DEAD!!";
		}
		request.setAttribute("reqMensaje", mensaje);
		return mensaje;
	}

}
