package filtro;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

/**
 * Filtro configurado para interceptar peticiones realizadas al Servlet
 */
@WebFilter("/crearPartida")
public class ValidarDatosServlet implements Filter {

	public void destroy() {
		System.out.println("Filtro finalizado");
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// Si queremos que la peticion interceptada por el filtro
		// progrese (pase al siguiente filtro si lo hay o al recurso
		// asociado al filtro hay que hacer la siguiente llamada
		// chain.doFilter(request, response);

		// Convertir peticion a clase derivada
		HttpServletRequest requestHttp = (HttpServletRequest) request;

		String vista = "/index.jsp";
		
		String nombre = request.getParameter("nombre");
    	String mazmorra = request.getParameter("mazmorra");

		if (mazmorra.equals("1") || mazmorra.equals("2") ) {
			vista = "/crearPartida";
		} else  {
			vista = "error.jsp";
		}

		RequestDispatcher dispatcher = 
				request.getRequestDispatcher(vista);

		dispatcher.forward(request, response);

	}
	
	public void init(FilterConfig fConfig) throws ServletException {
		System.out.println("Filtro inicializado");
	}

}
