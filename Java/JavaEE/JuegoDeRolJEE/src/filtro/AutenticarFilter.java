package filtro;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

/**
 * Filtro configurado para interceptar peticiones realizadas al Servlet
 */
@WebFilter("AutenticarFilter")
public class AutenticarFilter implements Filter {

	public void destroy() {
		System.out.println("Filtro finalizado");
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest requestHttp = (HttpServletRequest) request;

		String vista = "/index.jsp";
		
		String nombre = request.getParameter("nombre");
		
		 String[] anArray;
		 
	        anArray = new String[5];
	        anArray[0] = "playerone";
	        anArray[1] = "playertwo";
	        anArray[2] = "playerThree";
	        anArray[3] = "playerfour";
	        anArray[4] = "playerfive";
	        
	        boolean prueba = true;
	        for (int i = 0; i < anArray.length; i++) {
	        	if(anArray[i] == nombre) {
	        		prueba=true;
	        	}else {
	        		prueba=false;
	        	}	
			}
		
	     if(prueba) {
	    	 vista = "errorAutenticacion.jsp";
	     }else {
	    	 vista = "crearPartida.jsp";
	     }
	        
		RequestDispatcher dispatcher = 
				request.getRequestDispatcher(vista);

		dispatcher.forward(request, response);

	}
	
	public void init(FilterConfig fConfig) throws ServletException {
		System.out.println("Filtro inicializado");
	}

}