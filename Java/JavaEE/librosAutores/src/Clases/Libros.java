package Clases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import conexion.JDBC;





public class Libros {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			// 2.- CARGAR CLASE CON LOS DRIVERS DE BBDD
			Class.forName(JDBC.DRIVERS);
			
		} catch (ClassNotFoundException e) {
			System.out.println("No se puede cargar la clase"
					+ " con los drivers de BBDD");
			
			System.exit(0);
		}

		// Conexion a BBDD
		Connection connection = null;
		
		
		try {
			// 3.- CREAR Y ESTABLECER CONEXION CON LA BBDD
			// Especificamos URL, USUARIO Y CLAVE
			connection = DriverManager.getConnection(
					JDBC.URL, JDBC.USER, JDBC.PASSWORD);
			
		} catch (SQLException e) {
			System.out.println("No se puede establecer conexion"
					+ " con BBDD");
			
			System.exit(0);
		}
		
		Statement statement = null;
				
		try {
			// 4.- CREAR OBJETO DE EJECUCION DE SQL
			// INSTRUCCIONES SQL => Statement
			statement = connection.createStatement();
			
		} catch (SQLException e) {
			System.out.println("No se puede crear objeto ejecucion"
					+ " SQL con BBDD");
			
			System.exit(0);
		}
		
		
		ResultSet rs = null;		
		try {
			// 5.- EJECUTAR CODIGO SQL
			// EXECUTEQUERY => SELECT
			// EXECUTEUPDATE => NO SELECT
			rs = statement.executeQuery(JDBC.SELECT);
		} catch (SQLException e) {
			System.out.println("No se puede ejecutar codigo "
					+ " SQL con BBDD");
			
			System.exit(0);
		}
		
		// 6.- PROCESAR RESULTADOS EJECUCION
		// Desplazamiento al primer registro del ResultSet
		
		
		try {			
			if(rs.next()) {	
				// Al menos hay un registro
				int codigo = 0;
				String titulo = null;	
				System.out.println("TODOS LOS LIBROS ---------------");
				do {
					
					codigo = rs.getInt(1); // No entra dentro de este if (pendiente revisar)
					titulo = rs.getString(2);
					
					// Mostrar datos en pantalla
					System.out.println(codigo + "\t" + titulo);
					
				} while(rs.next()); // Volver a ejecutar mientras haya registros
			} else {
				System.out.println("No se han devuelto registros -- AQUI");
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		////METODOS PARA VER LIBROS POR AUTOR
		 rs = null;		
		try {
			// 5.- EJECUTAR CODIGO SQL
			// EXECUTEQUERY => SELECT
			// EXECUTEUPDATE => NO SELECT
			rs = statement.executeQuery(JDBC.SELECT_AUTORES_LIBRO);
		} catch (SQLException e) {
			System.out.println("No se puede ejecutar codigo "
					+ " SQL con BBDD en la segunda consulta");
			
			System.exit(0);
		};
		
		try {	
			
			
			if(rs.next()) {	
				// Al menos hay un registro
				int codigo = 0;
				String titulo = null;	
				String autor = null;
				System.out.println("LIBROS POR AUTOR ---------------");
				do {
					
					codigo = rs.getInt(1); // No entra dentro de este if (pendiente revisar)
					titulo = rs.getString(2);
					autor = rs.getString("ID_AUTORFK");
					
					// Mostrar datos en pantalla
					System.out.println(codigo + "\t" + titulo +"\t" + autor);
					
				} while(rs.next()); // Volver a ejecutar mientras haya registros
			} else {
				System.out.println("No se han devuelto registros -- AQUI");
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//UPDATE CON ROLLBACK
		
		try {
			
			PreparedStatement ps = connection.prepareStatement(
		"INSERT INTO libros (id_libro, titulo, id_autorfk) VALUES (?,?,?)");
			ps.setInt(13,5);
			ps.setString(14,"libro");
			ps.setInt(15,2);
			ps.executeUpdate();
			
			System.out.println("EJECUTADO");
			
		} catch (SQLException e1) {
			try {
				connection.rollback();
				
				System.out.println("ROLLBACK!");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// TODO Auto-generated catch block
			e1.printStackTrace();
		
		}
		// 7.- CERRAR CONEXION Y LIBERAR RECURSOS UTILIZADOS
		if(connection!=null) {
			try {
				connection.close();
			} catch (SQLException e) {
				System.out.println("No se puede cerrar conexion "
						+ " con BBDD");
			}
		}
	}

}
